element = FiniteElement("Lagrange", tetrahedron, 1)
v = TestFunction(element)
u = TrialFunction(element)

# case 5
a = ( v.dx(0) * u.dx(0) + v.dx(1) * u.dx(1) + v.dx(2) * u.dx(2) ) * dx
form = compile_form( a )

f = open ("form.h",'w')
for l in form : f.write(l)


#  tetrahedron = PQRS
#  P ( coord[0], coord[1],  coord[2]  )
#  Q ( coord[3], coord[4],  coord[5]  )
#  R ( coord[6], coord[7],  coord[8]  )
#  S ( coord[9], coord[10], coord[11] )

    PQ_x = J_c0 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[3] * FE8_C0_D001_Q1[0][0][1];
    PQ_y = J_c3 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[4] * FE8_C0_D001_Q1[0][0][1];
    PQ_z = J_c6 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[5] * FE8_C0_D001_Q1[0][0][1];
    PR_x = J_c1 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[6] * FE8_C0_D001_Q1[0][0][1];
    PR_y = J_c4 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[7] * FE8_C0_D001_Q1[0][0][1];
    PR_z = J_c7 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[8] * FE8_C0_D001_Q1[0][0][1];
    PS_x = J_c2 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[9] * FE8_C0_D001_Q1[0][0][1];
    PS_y = J_c5 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[10] * FE8_C0_D001_Q1[0][0][1];
    PS_z = J_c8 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[11] * FE8_C0_D001_Q1[0][0][1];
    
    sp[0] = PR_y * PS_z
    sp[1] = PS_y * PR_z
    PRSyz = sp[2] = sp[0] - sp[1] = PR_y * PS_z - PS_y * PR_z
    sp[3] = PQ_x * PRSyz
    sp[4] = PS_y * PQ_z
    sp[5] = PQ_y * PS_z
    det_PQSyz = sp[6] = sp[4] - sp[5] = PS_y * PQ_z - PQ_y * PS_z
    sp[7] = PR_x * det_PQSyz
    sp[8] = sp[3] + sp[7] = PQ_x * PRSyz + PR_x * det_PQSyz
    sp[9] = PQ_y * PR_z
    sp[10] = PR_y * PQ_z
    det_PQRyz = sp[11] = sp[9] - sp[10] = PQ_y * PR_z - PR_y * PQ_z
    sp[12] = PS_x * det_PQRyz
    det = sp[13] = sp[8] + sp[12] = PQ_x * PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz
    sp[14] = sp[2] / sp[13] = PRSyz / det
    sp[15] = - PQ_y * PS_z
    det_PQSyz = sp[16] = sp[4] + sp[15] = PS_y * PQ_z - PQ_y * PS_z
    sp[17] = sp[16] / sp[13] = det_PQSyz / det
    sp[18] = sp[11] / sp[13] = det_PQRyz / det
    sp[19] = sp[14] * sp[14] = PRSyz^2 / det^2
    sp[20] = sp[14] * sp[17] = PRSyz * det_PQSyz / det^2
    sp[21] = sp[18] * sp[14] = PRSyz * det_PQRyz / det^2
    sp[22] = sp[17] * sp[17] = det_PQSyz^2 / det^2
    sp[23] = sp[18] * sp[17] = det_PQSyz * det_PQRyz / det^2
    sp[24] = sp[18] * sp[18] = det_PQRyz^2 / det^2
    sp[25] = PS_x * PR_z
    sp[26] = - PS_z * PR_x
    det_PRSxz = sp[27] = sp[25] + sp[26] = PS_x * PR_z - PS_z * PR_x
    sp[28] = sp[27] / sp[13] = det_PRSxz / det
    sp[29] = PQ_x * PS_z
    sp[30] = - PQ_z * PS_x
    det_PQSxz = sp[31] = sp[29] + sp[30] = PQ_x * PS_z - PQ_z * PS_x
    sp[32] = sp[31] / sp[13] = det_PQSxz / det
    sp[33] = PR_x * PQ_z
    sp[34] = PQ_x * PR_z
    det_PQRxz = sp[35] = sp[33] - sp[34] = PR_x * PQ_z - PQ_x * PR_z
    sp[36] = sp[35] / sp[13] = det_PQRxz /det
    sp[37] = sp[28] * sp[28] = det_PRSxz^2 / det^2
    sp[38] = sp[28] * sp[32] = det_PRSxz * det_PQSxz / det^2
    sp[39] = sp[28] * sp[36] = det_PRSxz * det_PQRxz /det^2
    sp[40] = sp[32] * sp[32] = det_PQSxz^2 / det^2
    sp[41] = sp[32] * sp[36] = det_PQSxz * det_PQRxz /det^2
    sp[42] = sp[36] * sp[36] = det_PQRxz^2 /det^2
    sp[43] = sp[37] + sp[19] 
    sp[44] = sp[38] + sp[20]
    sp[45] = sp[39] + sp[21]
    sp[46] = sp[40] + sp[22]
    sp[47] = sp[41] + sp[23]
    sp[48] = sp[24] + sp[42]
    sp[49] = PR_x * PS_y;
    sp[50] = PS_x * PR_y;
    sp[51] = sp[49] - sp[50];
    sp[52] = sp[51] / sp[13];
    sp[53] = PS_x * PQ_y;
    sp[54] = PQ_x * PS_y;
    sp[55] = sp[53] - sp[54];
    sp[56] = sp[55] / sp[13];
    sp[57] = PQ_x * PR_y;
    sp[58] = PR_x * PQ_y;
    sp[59] = sp[57] - sp[58];
    sp[60] = sp[59] / sp[13];
    sp[61] = sp[52] * sp[52];
    sp[62] = sp[52] * sp[56];
    sp[63] = sp[60] * sp[52];
    sp[64] = sp[56] * sp[56];
    sp[65] = sp[60] * sp[56];
    sp[66] = sp[60] * sp[60];
    sp[67] = sp[43] + sp[61];
    sp[68] = sp[44] + sp[62];
    sp[69] = sp[45] + sp[63];
    sp[70] = sp[46] + sp[64];
    sp[71] = sp[47] + sp[65];
    sp[72] = sp[48] + sp[66];
	 assert ( det > 0. );
    sp[73] = std::abs(sp[13]);
    sp[74] = sp[67] * sp[73];
    sp[75] = sp[68] * sp[73];
    sp[76] = sp[69] * sp[73];
    sp[77] = sp[70] * sp[73];
    sp[78] = sp[71] * sp[73];
    sp[79] = sp[72] * sp[73];

so, if I understand correctly, FFC first divides 9 quantities by det
then multiplies 6 (derived from those 9) by abs(det)
what a waste of execution time !

    A[0] = 0.1666666666666667 * sp[74] + 0.1666666666666667 * sp[75] + 0.1666666666666667 * sp[76] + 0.1666666666666667 * sp[75] + 0.1666666666666667 * sp[77] + 0.1666666666666667 * sp[78] + 0.1666666666666667 * sp[76] + 0.1666666666666667 * sp[78] + 0.1666666666666667 * sp[79];
    A[1] = -0.1666666666666667 * sp[74] + -0.1666666666666667 * sp[75] + -0.1666666666666667 * sp[76];
    A[2] = -0.1666666666666667 * sp[75] + -0.1666666666666667 * sp[77] + -0.1666666666666667 * sp[78];
    A[3] = -0.1666666666666667 * sp[76] + -0.1666666666666667 * sp[78] + -0.1666666666666667 * sp[79];
    A[4] = -0.1666666666666667 * sp[74] + -0.1666666666666667 * sp[75] + -0.1666666666666667 * sp[76];
    A[5] = 0.1666666666666667 * sp[74];
    A[6] = 0.1666666666666667 * sp[75];
    A[7] = 0.1666666666666667 * sp[76];
    A[8] = -0.1666666666666667 * sp[75] + -0.1666666666666667 * sp[77] + -0.1666666666666667 * sp[78];
    A[9] = 0.1666666666666667 * sp[75];
    A[10] = 0.1666666666666667 * sp[77];
    A[11] = 0.1666666666666667 * sp[78];
    A[12] = -0.1666666666666667 * sp[76] + -0.1666666666666667 * sp[78] + -0.1666666666666667 * sp[79];
    A[13] = 0.1666666666666667 * sp[76];
    A[14] = 0.1666666666666667 * sp[78];
    A[15] = 0.1666666666666667 * sp[79];
  }

};



#endif
