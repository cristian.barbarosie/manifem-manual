

% \documentclass{report}

% \documentclass[a4paper,oneside]{scrbook}
% \pagestyle{plain}     % search for 'matter'

\documentclass[a4paper]{scrreprt}

\usepackage{typearea}
\areaset[1cm]{158mm}{234mm}
% \usepackage{scrlayer-scrpage}

\def\manifemrelease{25.02}
\def\manualversion {25.02}
\let\production 0  % 0: low-res figures, 1: high-res figures
\includeonly{manual-cap-01,index,changelog}
% \usepackage{showkeys}

\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{psfrag}
\usepackage{caption}
\usepackage{subcaption}

% \usepackage{fancybox}
\usepackage{fancyvrb}
% \renewcommand\VerbatimFont{\tiny\tt}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{indentfirst}
\if\production 1
\usepackage[h]{esvect}
\else
\let\vv\overrightarrow
\fi

\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
  \edef\arraystretch{#1}%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols c}}
\makeatother

% \usepackage{geometry}
% \geometry { a4paper  }

% \usepackage{lmodern}
% \ttfamily\fontseries{l}\selectfont light text
% \ttfamily\fontseries{m}\selectfont normal text
% \fontseries{b}\selectfont bold text

% \def\numb #1 {}
\def\numb{}
\usepackage{tocbasic}
\DeclareTOCStyleEntry[dynindent=on]{default}{section}

\newcommand\ManiFEM{\leavevmode\hbox{\includegraphics[width=13mm]{manifem-large}}}
\newcommand\maniFEM{\leavevmode\hbox{\includegraphics[width=13mm]{manifem-small}}}

\definecolor{manif}{rgb}{0., 0., 0.6}
\definecolor{tag}{rgb}{0.07, 0.07, 0.3}
\definecolor{tag}{rgb}{0.1, 0.1, 0.4}
\definecolor{coment}{rgb}{0.4, 0.4, 0.4}
\definecolor{comentsec}{rgb}{0.6, 0.6, 0.6}
\definecolor{moldura}{rgb}{0.3, 0.3, 0.3}
\definecolor{nova}{rgb}{0.35, 0., 0.2}
\definecolor{incl}{rgb}{0.3, 0.2, 0.1}
\definecolor{string}{rgb}{0.1, 0.25, 0.}
\definecolor{textindraw}{rgb}{0., 0.37, 0.}
\definecolor{numero}{rgb}{0.45, 0.25, 0.}
% \definecolor{nova}{rgb}{0., 0., 0.8}
% \definecolor{coment}{rgb}{0.5, 0.5, 0.5}
% \definecolor{manif}{rgb}{0.6, 0.1, 0.1}
% \definecolor{numero}{rgb}{0.7, 0.4, 0.}
\newcommand\azul[1]{\textcolor{nova}{#1}}
\newcommand\cinzasec[1]{\textcolor{comentsec}{#1}}
\newcommand\cinza[1]{\textcolor{coment}{#1}}
\newcommand\verm[1]{\textcolor{manif}{#1}}
\newcommand\verde[1]{\textcolor{string}{#1}}
\newcommand\laranja[1]{\textcolor{numero}{#1}}
\newcommand\taag{\textcolor{tag}{tag}}

\newcommand\tt{\normalfont\ttfamily}



\title{\includegraphics[width=12cm]{manifem-grey-capital.eps}}
\subtitle{USER'S MANUAL}
\author{Cristian Barbarosie and Anca-Maria Toader}
\date{\small version of this manual: \manualversion\\ documents \maniFEM\ release \manifemrelease}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

% \frontmatter
\begin{titlepage}
\maketitle
\end{titlepage}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter*{}

This document describes \maniFEM, a {\tt C++} library for generating meshes
and solving partial differential equations through the finite element method.
The name comes from ``finite elements on manifolds''. 
{\ManiFEM} was designed with the goal of coping with very general meshes,
in particular meshes on Riemannian manifolds, even manifolds which cannot be embedded
in $ {\mathbb R}^3 $, like the flat torus $ {\mathbb R}^2/{\mathbb Z}^2 $.
Also, {\maniFEM} was written with the goal of being conceptually clear
and easy to read.
We hope it will prove particularly useful for people who want fine control on the mesh,
e.g.\ for implementing their own meshing or remeshing algorithms.

{\ManiFEM} is just a collection of {\tt C++} classes.
It has no user-friendly interface nor graphic capabilities.
The user should have some understanding of programming and of {\tt C++}.
However, {\maniFEM} can be used at a basic level by people with no deep knowledge of
{\tt C++}.
A gallery of examples is available at {\small\tt https://maniFEM.rd.ciencias.ulisboa.pt/gallery/}
\hfil\break
In paragraphs \ref{\numb section 1.\numb parag 11}\,--\,\ref{\numb section 1.\numb parag 13}
of this manual, you can find three lists showing {\maniFEM}'s competitors,
as well as strong and weak points.

In its current version, release \manifemrelease, {\maniFEM} works well for mesh generation,
including meshes on quotient manifolds.
Non-uniform meshes can be built (paragraphs \ref{\numb section 3.\numb parag 23}\,--\,%
\ref{\numb section 3.\numb parag 26}), as well as anisotropic meshes
(paragraphs \ref{\numb section 3.\numb parag 27} - \ref{\numb section 3.\numb parag 30}).
Lagrange finite elements of degree one and two are implemented for triangular and quadrangular cells;
Lagrange finite elements of degree one are implemented for tetrahedral and hexahedral
(parallelipiped, cube) cells; many other types of finite elements are still to be implemented.
Variational formulations, implemented as {\tt C++} objects, allow for compact and elegant code
(section \ref{\numb section 6}).
A changelog is included at the end of this manual, before the index.
To check which version of {\maniFEM} is installed in your computer,
see at the beginning of the file {\small\tt maniFEM.h}.

A component of \maniFEM, {\small\tt\verm{MetricTree}}, can be used independently.
It is a generalization of quad-trees for metric spaces.
See paragraph \ref{\numb section 13.\numb parag 14}.

When finite element computations are enabled,
\maniFEM{} uses the {\small\tt Eigen} library for storing matrices and for solving systems of
linear equations, see {\small\tt https://eigen.tuxfamily.org/}\hfil\break
Many drawings in this manual have been produced using {\small\tt gmsh},
see {\small\tt https://gmsh.info/}

{\ManiFEM} is being developed by Cristian Barbarosie\,%
\footnote {\small\tt cristian.barbarosie@gmail.com}
and Anca-Maria Toader.
% Their work is supported by National Funding from FCT -- Funda\c c\~ao para a Ci\^encia e a
% Tecnologia (Portugal), through Faculdade de Ci\^encias da Universidade de Lisboa and
% Centro de Matem\'atica, Aplica\c c\~oes Fundamentais e Investiga\c c\~ao Operacional.%
% \footnote{project UID/MAT/04561/2020}

{\ManiFEM} is free software; it is copyrighted by Cristian Barbarosie
under the GNU Lesser General Public Licence.

The home page of {\maniFEM} is {\small\tt https://maniFEM.rd.ciencias.ulisboa.pt}
(where this manual can be found).

To use \maniFEM, visit {\small\tt https://codeberg.org/cristian.barbarosie/maniFEM},
choose a release and download all files to some directory in your computer.
Current code may be unstable, releases are stable.
You can then run the examples in this manual\,:
just {\small\tt make} {\small\tt run-parag-\ref{\numb section 1.\numb parag 1}}
for the example in paragraph \ref{\numb section 1.\numb parag 1},
{\small\tt make} {\small\tt run-parag-\ref{\numb section 2.\numb parag 16}}
for the example in paragraph \ref{\numb section 2.\numb parag 16}, and so on.
For graphic representations, you may want to use {\small\tt gmsh}, see {\small\tt https://gmsh.info/}

Finite elements are disabled by default in \maniFEM.
To enable finite element computations you must comment out (or delete) the line containing
{\small\tt -DMANIFEM\_NO\_FEM} in the {\small\tt Makefile}; you must also install the
{\small\tt Eigen} library.
Paragraph \ref{\numb section 12.\numb parag 15} gives more details.
\vfil\eject


\section*{Structure of this manual}

This manual is divided in parts and sections describing {\maniFEM} with increasing degree
of technical detail.

\subparagraph*{\ref{\numb section 1}.\hskip 8pt General description}
A quick overview of \maniFEM's capabilities.

\subparagraph*{Part I.~~Basic usage}
Sections \ref{\numb section 2} to \ref{\numb section 7} show how {\maniFEM} can be used
for building meshes and solving PDEs using high-level commands, in a style somewhat similar
to {\small\tt FreeFEM} or {\small\tt fenics}.
In this approach, \maniFEM{} can be used by people with no deep knowledge of {\tt C++}.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 2}.\hskip 10pt Grid meshes; patchwork}
Shows how to build structured meshes then join them like patches, some of them on manifolds.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 3}.\hskip 10pt Frontal mesh generation; knitting}
Shows how to build meshes starting from their boundary alone, some of them on manifolds.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 4}.\hskip 10pt Extruded meshes; sliding swatches}
Shows how to build meshes by sliding a mesh along another one.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 5}.\hskip 10pt Composite meshes}
Useful for writing and reading information to and from files, also for joining extruded meshes.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 6}.\hskip 10pt Functions and variational formulations}
Some details about functions, some examples of solving PDEs using variational formulations.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 7}.\hskip 10pt Quotient manifolds}
Describes meshes on quotient manifolds.


\subparagraph*{\ref{\numb section 8}.\hskip 10pt Mathematical details}
Gives informations which do not fit in other sections of the manual.


\subparagraph*{Part II.~~Advanced Usage}
Sections \ref{\numb section 9} and \ref{\numb section 10} show how {\maniFEM} can be used
for controlling the details of the mesh. We hope it will be particularly useful for
people interested in implemented their own meshing or remeshing algorithms.
Section \ref{\numb section 11} shows how to use finite elements using low-level constructions.
  
\vskip -27pt{~}
\subparagraph*{\ref{\numb section 9}.\hskip 10pt Cells, meshes, iterators}
Gives details about neighbourhood relations between cells in a mesh, orientation and iterators.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 10}.\hskip 5pt Remeshing}
Some simple examples showing how to manipulate a mesh.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 11}.\hskip 5pt Finite elements and integrators}
Shows examples of finite element computations.


\subparagraph*{Part III.~~For programmers}
Sections \ref{\numb section 12} and \ref{\numb section 13}
are aimed at people interested in maintaining and developing {\maniFEM}.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 12}.\hskip 5pt Technical details}
Various implementation details, programming style, compilation options, frequent errors.

\vskip -27pt{~}
\subparagraph*{\ref{\numb section 13}.\hskip 5pt Internal details}
Mainly drawings intended to support the developer in understanding the source code.


\subparagraph*{\ref{\numb section 14}.\hskip 5pt Frequently asked questions} \hglue 2cm
\vfil\eject


\section*{Aknowledgements}

Development of {\maniFEM} is supported by national funding from FCT -- {\slshape Funda\c c\~ao
para a Ci\^encia e a Tecnologia} (Portugal), through {\slshape Faculdade de Ci\^encias da Universidade
de Lisboa} and {\slshape Centro de Matem\'atica, Aplica\c c\~oes Fundamentais e Investiga\c c\~ao
Operacional}, project UIDB/04561/2020.

Anabela Silva contributed with drawings and source text coloring.

Armando Machado contributed with comments in paragraph \ref{\numb section 8.\numb parag 2}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Colophon}

See file {\small\tt misc/colophon.md} at {\small\tt https://codeberg.org/cristian.barbarosie/maniFEM}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% \vfil\eject\vglue 5cm

%  first  edition  February 2021  ISBN 978-972-8394-30-1
%  second edition  October  2024  ISBN 978-972-8394-34-9
% https://kindlepreneur.com/isbn-bar-code-generator/
% https://cloudconvert.com/png-to-eps

% \vglue 60mm
% \includegraphics[width=35mm]{ISBN-code-5.eps}
% \hskip 10mm\raise 20mm\hbox{\vtop{\hsize 90mm\noindent  % raise 17mm
% Universidade de Lisboa
% \hfil\break Faculdade de Ci\^encias
% \hfil\break Departamento de Matem\'atica
% \hfil\break %\phantom{$\displaystyle\int$}
% \hfil\break second edition, October 2024}}

\vskip 60mm
\noindent
Copyright 2020 -- 2025 Cristian Barbarosie {\small\tt cristian.barbarosie@gmail.com}
\bigskip

\noindent
\includegraphics[width=35mm]{common-creatives-by.eps}\hskip5mm
\raise 9mm\hbox{\vtop{\hsize 110mm\noindent
This manual is licensed under the
\hfil\break Creative Commons Attribution 4.0 International License\,:
\hfil\break{\tt https://creativecommons.org/licenses/by/4.0/}}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \mainmatter

\include{manual-cap-01}

\setpartpreamble
{\vskip 2cm
  Sections \ref{\numb section 2} to \ref{\numb section 7} show how {\maniFEM} can be used
  for building meshes and solving PDEs using high-level commands, in a style somewhat similar
  to {\small\tt FreeFEM} or {\small\tt fenics}.
  In this approach, \maniFEM{} can be used by people with no deep knowledge of {\tt C++}.}
\part{Basic usage}

\include{manual-cap-02}
\include{manual-cap-03}
\include{manual-cap-04}
\include{manual-cap-05}
\include{manual-cap-06}
\include{manual-cap-07}

\include{manual-cap-08}


\setpartpreamble
{\vskip 2cm
  Sections \ref{\numb section 9} and \ref{\numb section 10} show how {\maniFEM} can be used
  for controlling the details of the mesh. We hope it will be particularly useful for
  people interested in implemented their own meshing or remeshing algorithms.
  Section \ref{\numb section 11} shows how to use finite elements using low-level constructions.}
\part{Advanced usage}

\include{manual-cap-09}
\include{manual-cap-10}
\include{manual-cap-11}

\setpartpreamble
{\vskip 2cm
  Sections \ref{\numb section 12} and \ref{\numb section 13} are aimed at people
  interested in maintaining and developing \maniFEM.
  Of course the ultimate documentation is the source code; these sections can be used as
  a guide through the source code.}
\part{For programmers}

\include{manual-cap-12}
\include{manual-cap-13}

\include{manual-cap-14}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \backmatter

\include{changelog}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\include{index}

\end{document}


% Herbert Edelsbrunner, Geometry and topology for mesh generation, Cambridge, University Press, 2001
