
           %---------------------------------%
\chapter {~~Extruded meshes; sliding swatches}\label{\numb section 4}
           %---------------------------------%

This section shows how to build a mesh by sliding a mesh (which we call ``swatch'')
along another mesh (which we call ``track'').
These two meshes must share one vertex (and no more than one).
The result is a higher-dimensional mesh; the (topological) dimension of the mesh thus built
is equal to the sum of the (topological) dimensions of the two involved meshes.
To the mathematically trained reader, this operation reminds a cartesian product.

Curvature effects can be taken into effect as shown in paragraphs \ref{\numb section 4.\numb parag 3}
-- \ref{\numb section 4.\numb parag 8}.
In the future, torsional effects will be also taken into account.

The orientation of the resulting mesh is discussed in paragraph \ref{\numb section 4.\numb parag 10}.

See also paragraph \ref{\numb section 2.\numb parag 23} for a parametric approach.

Paragraphs \ref{\numb section 5.\numb parag 7}, \ref{\numb section 5.\numb parag 8}
and \ref{\numb section 9.\numb parag 4} show how the extrusion technique can be used with
{\small\tt\verm{Mesh}::Composite} objects for building more complex meshes.


          %---------------%
\section{~~A parallelogram}\label{\numb section 4.\numb parag 1}
          %---------------%

Here is one of the simplest examples of an extruded mesh.
Both the swatch (drawn in blue) and the track (drawn in red) are straight line segments.

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{O}{\small\tt\textcolor{textindraw}{O}}
  \includegraphics[width=85mm]{square-extrude}
  \caption{extruded mesh, both swatch and track are straight segments}
  \label{\numb section 4.\numb fig 1}
\end{figure}

A {\small\tt\verm{Mesh}::Build}\,er with {\small\tt\taag::extrude} is used.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 4.\numb parag 1}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh} \azul{OA} = \verm{Mesh}::Build ( \taag::grid ) .shape ( \taag::segment )
      .start_at ( O ) .stop_at ( A ) .divided_in ( \laranja{10} );
   \verm{Mesh} \azul{OB} = \verm{Mesh}::Build ( \taag::grid ) .shape ( \taag::segment )
      .start_at ( O ) .stop_at ( B ) .divided_in ( \laranja{10} );

   \verm{Mesh} \azul{rhombus} = \verm{Mesh}::Build ( \taag::extrude ) .swatch ( OB ) .slide_along ( OA );
\end{Verbatim}

Of course the same mesh could have been built using a {\small\tt\verm{Mesh}::Build}\,er with
{\small\tt\taag::grid}, starting from its four sides or directly from its four vertices;
see paragraph \ref{\numb section 1.\numb parag 4} for instance.

In this example, the roles of the swatch and of the track are interchangeable.
If we switch them, we obtain the same {\small\tt rhombus}, although with opposite orientation;
see paragraph \ref{\numb section 4.\numb parag 10}.


          %------------------------------------%
\section{~~Non-uniform distribution of vertices}\label{\numb section 4.\numb parag 2}
          %------------------------------------%

Suppose we want to refine the mesh in a corner of a rectangle, while keeping a grid-like
structure of the mesh.
This is easy to do by extrusion, see figure \ref{\numb section 4.\numb fig 2}.
It is not possible to obtain such a mesh using a {\small\tt\verm{Mesh}::Build}\,er with
{\small\tt\taag::grid}; see paragraph \ref{\numb section 2.\numb parag 4}.

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{C}{\small\tt\textcolor{textindraw}{C}}
  \includegraphics[width=100mm]{rectangle-non-unif-extrude}
  \caption{extruded mesh, non-uniform distribution of vertices}
  \label{\numb section 4.\numb fig 2}
\end{figure}

We see that the common vertex does not have to be the first vertex of the track
(or of the swatch, actually); it can be the last vertex.
It can even be an inner vertex, as shown in paragraphs \ref{\numb section 4.\numb parag 3}
-- \ref{\numb section 4.\numb parag 5}.

Again, the roles of the swatch and of the track are interchangeable.
If we switch them, we obtain the same mesh, although with opposite orientation;
see paragraph \ref{\numb section 4.\numb parag 10}.

Paragraph \ref{\numb section 5.\numb parag 8} shows how the extrusion technique can interact with
{\small\tt\verm{Mesh}::Composite} objects for building more complex meshes.


          %------------%
\section{~~Curved lines}\label{\numb section 4.\numb parag 3}
          %------------%

Both swatch and track may be curved.
The common vertex does not need to be an extremity of any of these lines.

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{C}{\small\tt\textcolor{textindraw}{C}}
  \psfrag{D}{\small\tt\textcolor{textindraw}{D}}
  \includegraphics[width=100mm]{gelly-01}
  \caption{curved swatch, curved track}
  \label{\numb section 4.\numb fig 3}
\end{figure}

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 4.\numb parag 3}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Cell} O ( \taag::vertex);  x(O) = \laranja{0.};    y(O) =  \laranja{0.};
   \verm{Cell} A ( \taag::vertex);  x(A) = \laranja{0.2};   y(A) = \laranja{-1.};
   \verm{Cell} B ( \taag::vertex);  x(B) = \laranja{0.2};   y(B) =  \laranja{1.};

   RR2 .implicit ( x == \laranja{0.2}*y*y );
   \verm{Mesh} \azul{OA} = \verm{Mesh}::Build ( \taag::grid ) .shape ( \taag::segment )
      .start_at ( O ) .stop_at ( A ) .divided_in ( \laranja{10} );
   \verm{Mesh} \azul{OB} = \verm{Mesh}::Build ( \taag::grid ) .shape ( \taag::segment )
      .start_at ( O ) .stop_at ( B ) .divided_in ( \laranja{10} );
   \verm{Mesh} \azul{AB} = \verm{Mesh}::Build ( \taag::join ) .mesh ( OA .reverse() ) .mesh ( OB );

   RR2 .implicit ( y == \laranja{0.5}*sin(x) );
   \verm{Cell} C ( \taag::vertex, \taag::of_coords, \{ -pi, \laranja{0.} \}, \taag::project );
   \verm{Cell} D ( \taag::vertex, \taag::of_coords, \{ pi/\laranja{2.}, \laranja{0.} \}, \taag::project );
   \verm{Mesh} \azul{OC} = \verm{Mesh}::Build ( \taag::grid ) .shape ( \taag::segment )
      .start_at ( O ) .stop_at ( C ) .divided_in ( \laranja{20} );
   \verm{Mesh} \azul{OD} = \verm{Mesh}::Build ( \taag::grid ) .shape ( \taag::segment )
      .start_at ( O ) .stop_at ( D ) .divided_in ( \laranja{10} );
   \verm{Mesh} \azul{CD} = \verm{Mesh}::Build ( \taag::join ) .mesh ( OC .reverse() ) .mesh ( OD );

   RR2 .set_as_working_manifold();
   \verm{Mesh} \azul{gelly} = \verm{Mesh}::Build ( \taag::extrude ) .swatch ( AB ) .slide_along ( CD );
\end{Verbatim}

Again, the roles of the swatch and of the track are interchangeable.
If we switch them, we obtain the same mesh, although with opposite orientation;
see paragraph \ref{\numb section 4.\numb parag 10}.


          %-----------------------%
\section{~~Following the curvature}\label{\numb section 4.\numb parag 4}
          %-----------------------%

If we invoke method {\small\tt follow\_\hglue 0.5pt curvature}, \maniFEM{} will rotate
the swatch according to the curvature of the track.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 4.\numb parag 4}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh} \azul{gelly} = \verm{Mesh}::Build ( \taag::extrude ) .swatch ( AB ) .slide_along ( CD )
      .follow_curvature();
\end{Verbatim}

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{C}{\small\tt\textcolor{textindraw}{C}}
  \psfrag{D}{\small\tt\textcolor{textindraw}{D}}
  \includegraphics[width=120mm]{gelly-02}
  \caption{following the curvature of {\small\tt CD}}
  \label{\numb section 4.\numb fig 4}
\end{figure}

Unlike in previous paragraphs, 
here if we switch the roles of the swatch and of the track we get a different extruded mesh;
compare with paragraph \ref{\numb section 4.\numb parag 5}.
See also paragraph \ref{\numb section 4.\numb parag 10} with regard to orientations.


          %------------------------------%
\section{~~Following the curvature, again}\label{\numb section 4.\numb parag 5}
          %------------------------------%

When curvature is taken into account, switching the roles of
the swatch and of the track produces a different extruded mesh.%
\footnote{{} An exception is when both swatch and track are straight line segments,
like in paragraphs \ref{\numb section 4.\numb parag 1} and \ref{\numb section 4.\numb parag 2};
in this case invoking method {\footnotesize\tt follow\_curvature} has no effect, of course.}

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{C}{\small\tt\textcolor{textindraw}{C}}
  \psfrag{D}{\small\tt\textcolor{textindraw}{D}}
  \includegraphics[width=79mm]{gelly-03}
  \caption{following the curvature of {\small\tt AB}}
  \label{\numb section 4.\numb fig 5}
\end{figure}

Compare with paragraph \ref{\numb section 4.\numb parag 4}.
See also paragraph \ref{\numb section 4.\numb parag 10} with regard to orientations.


          %---------------------------%
\section{~~Sliding along a closed loop}\label{\numb section 4.\numb parag 6}
          %---------------------------%

If we choose a closed loop as {\small\tt track} and follow its curvature, we get an annulus.

\begin{figure}[ht] \centering
  \includegraphics[width=80mm]{annulus}
  \caption{sliding a parabola along a circle}
  \label{\numb section 4.\numb fig 6}
\end{figure}


          %-------------------------------------%
\section{~~Swatch and track in orthogonal planes}\label{\numb section 4.\numb parag 7}
          %-------------------------------------%

Here we consider the same circular {\small\tt track} as in paragraph \ref{\numb section 4.\numb parag 6},
but we use as {\small\tt swatch} a parabola in a plane orthogonal to the circle.

\begin{figure}[ht] \centering
  \includegraphics[width=80mm]{rim}
  \caption{a wheel rim}
  \label{\numb section 4.\numb fig 7}
\end{figure}


          %---------------%
\section{~~Rotating a star}\label{\numb section 4.\numb parag 8}
          %---------------%

Both {\small\tt swatch} and {\small\tt track} may be closed loops.
This is not interesting in $ \mathbb R^2 $; in $ \mathbb R^3 $ produces meshes similar to a torus.
Such a doughnut is built in paragraph \ref{\numb section 5.\numb parag 4}.
In the present paragraph we use a star-shaped swatch
(presented in paragraph \ref{\numb section 1.\numb parag 8})
and obtain the surface shown in figure \ref{\numb section 4.\numb fig 8}.

\begin{figure}[ht] \centering
  \includegraphics[width=90mm]{torus-extrude}
  \caption{a fancy doughnut}
  \label{\numb section 4.\numb fig 8}
\end{figure}


          %----------------------%
\section{~~Two-dimensional swatch}\label{\numb section 4.\numb parag 9}
          %----------------------%

We can slide a two-dimensional mesh along a one-dimensional track (be it closed or open).
The result will be a volumic mesh, made of cubes if the swatch is made of squares,
made of prisms if the swatch is made of triangles.

In the present paragraph we use the star built in paragraph \ref{\numb section 1.\numb parag 9}
(made of triangles) and let it slide along a helix.
As it slides, the swatch does not rotate according to the curvature of the track
(we do not invoke the method {\small\tt follow\_\hglue 0.5pt curvature}).

\begin{figure}[ht] \centering
  \includegraphics[width=88mm]{star-helix-01}
  \caption{two-dimensional swatch sliding along a helix}
  \label{\numb section 4.\numb fig 9}
\end{figure}

The extruded mesh (made of triangular prisms) is shown in figure \ref{\numb section 4.\numb fig 9}.

Here, it is difficult to predict whether the orientation of the extruded volumic mesh is
compatible or not with the intrinsic orientation of $ \mathbb R^3 $.
If the orientation is important for you, we recommend to perform a preliminary test and,
if the orientation is wrong, to simply revert one of the meshes\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 4.\numb parag 9}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh} \azul{tower} = \verm{Mesh}::Build ( \taag::extrude ) .swatch ( star )
                                             .slide_along ( EG .reverse() );
\end{Verbatim}


          %------------------------------%
\section{~~Orientation of extruded meshes}\label{\numb section 4.\numb parag 10}
          %------------------------------%

Consider two meshes {\small\tt msh\_a} and {\small\tt msh\_b} having one (and only one) vertex
in common.
Suppose it makes sense to use either one as a swatch and the other as track,
that is, suppose both operations below make sense.
\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,baselinestretch=0.94]
   \verm{Mesh} \azul{extruded_1} = \verm{Mesh}::Build ( \taag::extrude ) .swatch ( msh_a ) .slide_along ( msh_b );
   \verm{Mesh} \azul{extruded_2} = \verm{Mesh}::Build ( \taag::extrude ) .swatch ( msh_b ) .slide_along ( msh_a );
\end{Verbatim}

As previously mentioned, the shape of the two extruded meshes may be different due to curvature
and torsion effects.
But, even being different geometrically, they are similar enough (topologically equivalent)
so we can compare their orientations.

Due to the implementation of the extrusion algorithm in \maniFEM, the above built
meshes {\small\tt extruded\_\hglue 0.5 pt 1} and {\small\tt extruded\_\hglue 0.8 pt 2}
have opposite orientations.%
\footnote{{} This makes sense from the viewpoint of differential geometry.
If we take the product of two oriented manifolds, the orientation of the product manifold
depends on the order of the factors.}
If the orientation matters to you, you should take steps to ensure you get the right orientation.

Note that if we switch the orientation of one mesh the orientation of the extruded mesh changes.
Thus, among these meshes
\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,baselinestretch=0.94]
   \verm{Mesh} \azul{extruded_3} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( msh_a .reverse() ) .slide_along ( msh_b );
   \verm{Mesh} \azul{extruded_4} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( msh_a ) .slide_along ( msh_b .reverse() );
   \verm{Mesh} \azul{extruded_5} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( msh_a .reverse() ) .slide_along ( msh_b .reverse() );
   \verm{Mesh} \azul{extruded_6} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( msh_b .reverse() ) .slide_along ( msh_a );
   \verm{Mesh} \azul{extruded_7} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( msh_b ) .slide_along ( msh_a .reverse() );
   \verm{Mesh} \azul{extruded_8} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( msh_b .reverse() ) .slide_along ( msh_a .reverse() );
\end{Verbatim}
{\small\tt extruded\_\hglue 0.5 pt 1}, {\small\tt extruded\_\hglue 0.8 pt 5},
{\small\tt extruded\_\hglue 0.8 pt 6} and
{\small\tt extruded\_\hglue 0.5 pt 7} have the same orientation; {\small\tt extruded\_\hglue 0.8 pt 2},
{\small\tt extruded\_\,3}, {\small\tt extruded\_\,4} and {\small\tt extruded\_\,8}
also have the same orientation, opposite to the former.


