
          %--------------------%
\chapter{~~Mathematical details}\label{\numb section 8}
          %--------------------%

This section discusses miscellaneous mathematical aspects controlling the behaviour of \maniFEM.


          %---------------------------------%
\section{~~Projecting a vertex on a manifold}\label{\numb section 8.\numb parag 1}
          %---------------------------------%

We recall that, in this manual, we use the term ``manifold'' to refer to a manifold without boundary.
Of course we will only mesh a bounded domain of the manifold, so most meshes will have a boundary.
We may also mesh an entire compact manifold like a circle or a sphere or a torus;
such a mesh will have no boundary.

In many examples we consider a submanifold of $ \mathbb{R}^n $ defined by an implicit equation
(or by several implicit equations).
When meshing such a manifold (or a bounded domain of such a manifold),
we often need to project points in the surrounding space onto the working manifold.
This projection operation is achieved by solving the equation (or system of equations)
defining the submanifold.

If the manifold is defined by one or two implicit equations,
these equations are solved numerically by using a Newton-like algorithm.\,%
\footnote {{} see e.g.\ appendix B in the paper\,: C.~Barbarosie, A.M.~Toader, S.~Lopes,
A gradient-type algorithm for constrained optimization with application to microstructure optimization,
Discrete and Continuous Dynamical Systems series B, 25, p.\ 1729-1755, 2020}
For two equations, the inverse of the Jacobian matrix is computed by simply applying the definition
(quotients of 2x2 determinants).
If the manifold is defined by three implicit equations (often, this will be a zero-dimensional manifold),
we prefer not to compute the inverse of a 3x3 matrix, so we apply a conjugate gradient method instead.
These algoritms converge only if the initial guess (that is, the coordinates of the point we are
trying to project) is close enough to the manifold.
The user should take this limitation into account.

The case of four or more implicit equations is not implemented.

Many {\small\tt\verm{Mesh}} constructors project vertices on the current working manifold
{\small\tt\verm{Manifold}::working} wihtout the user's assistance.
On the other hand, vertices can be explicitly {\small\tt project}ed if necessary,
either directly within the {\small\tt\verm{Cell}} constructor or by invoking the method
{\small\tt\verm{Mesh}::project}.
Paragraphs \ref{\numb section 2.\numb parag 6}\,--\,\ref{\numb section 2.\numb parag 19},
\ref{\numb section 3.\numb parag 2}\,--\,\ref{\numb section 3.\numb parag 9} show such examples.


          %---------------------%
\section{~~Quotient manifolds}\label{\numb section 8.\numb parag 2}
          %---------------------%

Section \ref{\numb section 7} is devoted to meshes on quotient manifolds.

Usually, a quotient manifold is defined by means of orbits of the ``periodicity'' group.
The manifolds considered in paragraphs \ref{\numb section 7.\numb parag 14}\,--\,%
\ref{\numb section 7.\numb parag 18} cannot be defined this way if their angle is not a
divisor of $ 2\pi $.
This can be easily circumvented; it suffices to consider only a neighbourhood of a representative
domain instead of the entire $ \mathbb{R}^2 $.
In the case of a singular point (paragraphs \ref{\numb section 7.\numb parag 15} and
\ref{\numb section 7.\numb parag 16}) the above refered neighbourhood should not contain
the singularity.

When we divide a Riemannian manifold (in particular, the Euclidian space $ \mathbb{R}^n $)
by a group of isometries, the resulting (quotient) manifold inherits naturally a Riemann metric.
This is not true for the manifolds considered in paragraphs \ref{\numb section 7.\numb parag 17}
and \ref{\numb section 7.\numb parag 18} -- the actions are not isometries.
In other words, we identify a pair of sides (of the representative domain) having different lengths,
so the quotient operation is not compatible with the metric in $ \mathbb{R}^2 $.
Thus, these quotient manifolds do not inherit a Riemann metric.
They are valid as differentiable manifolds but not as Riemann manifolds.
Length and area are not well-defined in these examples.
Since they are compact, these manifolds can be viewed as uniform spaces.%
\footnote {{} see e.g.\ corollary 30 in chapter 6 of J.L.~Kelley, General Topology, Van Nostrad, 1955}


          %------------------------------%
\section{~~A rotation in $ \mathbb{R}^3 $}\label{\numb section 8.\numb parag 3}
          %------------------------------%

In paragraphs \ref{\numb section 4.\numb parag 4} -- \ref{\numb section 4.\numb parag 9}
we see the need to describe the curvature of a chain of segments.
This is done by measuring the rotation from each segment to the next one.

Consider two given vectors $ \vec u, \vec v \in \mathbb{R}^3 $, of norm $1$.
There are many rotations in $ \mathbb{R}^3 $ transforming $ \vec u $ \hglue 0.6pt into $ \vec v $.
There is, however, one particular rotation whose angle is the smallest possible\,:
it is achieved when the rotation axis is orthogonal to both $ \vec u $ \hglue 0.6pt and $ \vec v $.\;%
\footnote {{} We mean a proper rotation, one that preserves the orientation of $ \mathbb{R}^3 $.
That is, an element of $ SO(3) $.}
This paragraph shows how to compute this rotation matrix.
In paragraph \ref{\numb section 8.\numb parag 4} we dwell on the particular case when
the vectors $ \vec u $ and $ \vec v $ are nearly equal, that is, the rotation angle is near zero.

From a geometrical point of view, the path is clear.
Let us denote by $P$ the plane defined by $ \vec u $ \hglue 0.6pt and $ \vec v $.
One defines the rotation axis as being orthogonal to $P$
(using the cross product $ \vec u \times \vec v $).
The same cross product provides the sine of the rotation angle.
Alternatively, the inner product $ \vec u \cdot \vec v $ \hglue 0.6pt provides the cosine of the same angle.
Then one defines the rotation in the plane $P$, essentially as a two-dimensional rotation.
However, most of the existing literature focuses on expressing this rotation matrix in terms of
the direction of the rotation axis and of the rotation angle.%
\footnote {{} see K.K.~Liang, Efficient conversion from rotating matrix to rotation axis and angle by extending Rodrigues' formula, arXiv:1810.02999}\,%
\footnote {{} see R.~Friedberg, Rodrigues, Olinde: ``Des lois géométriques qui régissent les déplacements d'un système solide...'', translation and commentary, arXiv:2211.07787}
The computation of the matrix based on the two vectors
$ \vec u $ \hglue 0.6pt and $ \vec v $ \hglue 0.6pt is not discussed.
As we shall see in the sequel, when computing the rotation matrix based on the two vectors
$ \vec u $ \hglue 0.6pt and $ \vec v $ \hglue 0.6pt one encounters new challenges
and gains some insights.

The first challenge is conceptual (almost philosophical) in nature and is related to the rotation angle.
If we are looking for a rotation in $ \mathbb{R}^2 $, the requirement that $ \vec u $ \hglue 0.6pt should
be transported into $ \vec v $ \hglue 0.6pt clearly defines the angle of rotation.
The cross product $ \vec u \times \vec v $ \hglue 0.6pt can be interpreted as a real number
(wihch may be positive or negative) and this defines a (positive or negative) rotation angle.
All this is possible because $ \mathbb{R}^2 $ has a canonical orientation (which we call ``intrinsic''
in paragraphs \ref{\numb section 3.\numb parag 10} and \ref{\numb section 3.\numb parag 12}).

For a rotation in a plane $ P \subset \mathbb{R}^3 $ things are less clear because
(although $ \mathbb{R}^3 $ has its canonical, or intrinsic, orientation)
there is no canonical orientation of $P$.
The sign of the angle of rotation is not clearly defined.
The cross product $ \vec u \times \vec v $ \hglue 0.6pt is no longer a real number but a vector
in $ \mathbb{R}^3 $; its norm can be interpreted as the sine of the
rotation angle, but this provides always a positive value.
Using the cosine (given by the inner product $ \vec u \cdot \vec v $\hglue 0.6pt)
does not provide a signed angle either,
because the cosine function does not distinguish between positive and negative arguments.

Let us introduce the following notations (recall that $ \vec u $ \hglue 0.6pt and $ \vec v $ \hglue 0.6pt are given vectors
in $ \mathbb{R}^3 $, they have norm $1$ but are not orthogonal).
\[ c = \vec u \cdot \vec v \qquad \vec w = \vec u \times \vec v
\qquad s = \| \vec w \mbox{\hglue 0.6pt}\| \]
We are looking for a rotation matrix $R$, with axis $ \vec w $, such that $ R \vec u = \vec v $.
For an arbitrary $ \vec x \in \mathbb{R}^3 $, we are looking for an expression of $ R \vec x $.
Let us write $ \vec x $ as a linear combination of $ \vec u $, $ \vec v $ \hglue 0.6pt and
$ \vec w / s $.
These three vectors form a basis in $ \mathbb{R}^3 $, although not an orthonormal one
(they all have norm $1$, but the first two vectors are not orthogonal,
only the third one is orthogonal to the first two).
\[ \vec x = \alpha\, \vec u + \beta\, \vec v + \gamma \,\frac{\vec w}s \]

Denote by $ (r_{ij}) $ the components of the matrix representing the rotation $R$ in the
chosen (skew) basis, that is, 
\[ \begin{matrix}[2]
  R \vec u = r_{11}\, \vec u + r_{12}\, \vec v + r_{13} \displaystyle\frac{\vec w}s \\
  R \vec v = r_{21}\, \vec u + r_{22}\, \vec v + r_{23} \displaystyle\frac{\vec w}s \\
  R \displaystyle\frac{\vec w}s =
  r_{31}\, \vec u + r_{32}\, \vec v + r_{33} \displaystyle\frac{\vec w}s
\end{matrix} \]

Since we want $ R \vec w = \vec w $, we conclude that $ r_{31} = r_{32} = 0 $ and $ r_{33} = 1 $.
Since we want $ R \vec u $ and $ R \vec v $ to stay orthogonal to $ \vec w $,
we conclude that $ r_{13} = r_{23} = 0 $.
So we are left with finding the upper left $ 2\times 2 $ block of $R$.
On one hand, we want $ R \vec u = \vec v $ \hglue 0.6pt which implies $ r_{11} = 0 $, $ r_{12} = 1 $.
On the other hand, we want $R$ to preserve norms and angles; the condition $ R \vec u \cdot R \vec v =
\vec u \cdot \vec v $ \hglue 0.6pt implies $ c\, r_{21} + r_{22} = c $
while the condition $ \| R \vec v \,\| = 1 $ implies $ r_{21}^2 + r_{22}^2 + 2c\,r_{21} r_{22} = 1 $.
Simple computations lead to $ r_{21}^2 = 1 $.

If we choose $ r_{21} = 1 $ and thus $ r_{22} = 0 $, we get
\[ R\,(\alpha\, \vec u + \beta\, \vec v + \gamma \,\frac{\vec w}s) =
\beta\, \vec u + \alpha\, \vec v + \gamma \,\frac{\vec w}s \]
which represents a reflection in $ \mathbb{R}^3 $; not what we want.

So we are led to $ r_{21} = -1 $ and $ r_{22} = 2c $, thus
\[ R\,(\alpha\, \vec u + \beta\, \vec v + \gamma \,\frac{\vec w}s) =
-\beta\, \vec u + (\alpha+2c\beta)\, \vec v + \gamma \,\frac{\vec w}s \]

We have obtained a matricial representation of the rotation $R$ in the basis
$ \{ \vec u, \vec v, \vec w / s \} $ (which is not orthonormal).
We need to transform this matrix in order to obtain the representation of $R$
in the canonical basis of $ \mathbb{R}^3 $.

It is a mere exercise to express, for a given $ \vec x \in \mathbb{R}^3 $,
the coefficients $ \alpha, \beta $ and $ \gamma $ 
\[ \alpha = \frac {\vec x\cdot \vec u - c\,\vec x\cdot \vec v} {s^2}\quad
\beta = \frac {\vec x\cdot \vec v - c\,\vec x\cdot \vec u} {s^2}\quad
\gamma = \vec x \cdot \frac{\vec w}s \]
which leads to
\[ s^2 R\vec x = c\, (\vec x \cdot \vec u)\, \vec u 
- (\vec x \cdot \vec v)\, \vec u
+ (1-2c^2)\, (\vec x \cdot \vec u)\, \vec v 
+ c\, (\vec x \cdot \vec v)\, \vec v + (\vec x \cdot \vec w)\, \vec w  \]

The above gives a matricial expression of $R$ which is readily implemented numerically.
Returning to the philosophical remarks at the beginning of this paragraph,
we note that this expression does not depend on the sign of $s$;
this is a pleasant surprise.

Paragraph \ref{\numb section 8.\numb parag 4} presents a different expression of $R$
which may be more convenient than the one above.
\vfil\eject


          %------------------------------------%
\section{~~A small rotation in $ \mathbb{R}^3 $}\label{\numb section 8.\numb parag 4}
          %------------------------------------%

When trying to describe the curvature of a ``track'' (see paragraphs \ref{\numb section 4.\numb parag 4}
-- \ref{\numb section 4.\numb parag 9}), we need to compute the rotation matrix
from each segment of the ``track'' to the next one.
The rotation angle will be (the complementary of) the angle between these two segments.
If the ``track'' is smooth enough (and we usually want it to be smooth), this angle will be small.
The smoother the ``track'' is, the smaller the angle will be, so we must think of this
angle as approaching zero.

When the angle of rotation approaches zero, the computations presented in paragraph
\ref{\numb section 8.\numb parag 3} become numerically unstable.
Divisions by $s$ or $s^2$ are problematic and should be avoided.
To make things worse, we want to ``accumulate'' these infinitesimal rotations
by successive matrix multiplications,
in order to account for the global curvature of the ``track'', which is not infinitesimal.
So, any numerical errors in computing the individual, infinitesimal, rotations will
accumulate and may have disastrous effects when approaching the end of the ``track''.

Recall that $ \vec u $ and $ \vec v $ are given vectors or norm $1$ but are not orthogonal
(in the present paragraph they are nearly parallel);
we are looking for a rotation which transforms $ \vec u $ \hglue 0.6pt into $ \vec v $,
with rotation axis orthogonal to both $ \vec u $ and $ \vec v $.

Note that in $ \mathbb{R}^2 $ we have a numerically robust formula for the rotation matrix
\[ R = \begin{bmatrix} c & -s \\ s & c \end{bmatrix} \quad\mbox{where}
\quad \begin{matrix} c = \vec u \cdot \vec v = u_1 v_1 + u_2 v_2 \\
 s = \vec u \times \vec v = u_1 v_2 - u_2 v_1 \end{matrix} \]

Back to $ \mathbb{R}^3 $.
Before proceeding, we should ask ourselves whether it makes sense to even try to compute
such a rotation matrix when the angle is near zero.
After all, if the two vectors are (almost) equal, the rotation axis ceases to be well-defined.
On the other hand, our geometric intuition says that, if we fix the vector $ \vec u $ \hglue 0.6pt and
let the vector $ \vec v $ \hglue 0.6pt converge towards $ \vec u $,
the rotation matrix should converge to the identity matrix $ I_{3\times 3} $.
The axis of rotation may not converge to a specific direction, but the matrix should converge to
$ I_{3\times 3} $.
So, if we want to implement robust numerical computations, we should not use the axis of rotation
but rather the nine components of the matrix directly, and we should avoid divisions by
small quantities like $s$ or $ s^2 $.

By glancing at the last formula in paragraph \ref{\numb section 8.\numb parag 3} we see that,
indeed, this is our main challenge\,: to eliminate the need to divide by $ s^2 $.
In the same formula, there are other small quantities, just well hidden beneath the surface.
The vector $ \vec w $, for instance, will be small.
The fact that $ \vec u $ \hglue 0.6pt is very close to $ \vec v $ \hglue 0.6pt hints at another small quantity.
Let us make it visible by calling $ \vec d $ \hglue 0.6pt the difference, that is, by replacing $ \vec v $
\hglue 1pt by $ \vec u + \vec d $.
We get
\[ s^2 R\vec x = 2c\, (1-c)\, (\vec x \cdot \vec u)\, \vec u
- (1-c)\, (\vec x \cdot \vec d)\, \vec u
+ (1+c-2c^2)\, (\vec x \cdot \vec u)\, \vec d
+ c\, (\vec x \cdot \vec d)\, \vec d + (\vec x \cdot \vec w)\, \vec w  \]

The trigonometric identity $ 1-c = \displaystyle\frac {s^2}{1+c} $ comes very handy.
Together with $ 1+c-2c^2 = $ $ = (1-c)(1+2c) $, this allows us to get rid of the division by
$ s^2 $ in the first three parcels :
\[ R\vec x = \frac {2c}{1+c}\, (\vec x \cdot \vec u)\, \vec u
- \frac 1{1+c}\, (\vec x \cdot \vec d)\, \vec u
+ \frac{1+2c}{1+c}\, (\vec x \cdot \vec u)\, \vec d
+ \frac c{s^2}\, (\vec x \cdot \vec d)\, \vec d
+ \frac 1{s^2}\, (\vec x \cdot \vec w)\, \vec w  \]
In these first three parcels we have now a division by $ 1+c $ which is quite robust
unless $ \vec v $ approaches $ -\vec u $.\;%
\footnote {{} This never happens in our code; see comments at the end of the present paragraph.}

In the last two parcels we still have a division between two small quantities
(recall that $s$, $ \vec d $ and $ \vec w $ are small).
It is an interesting challenge to re-write these two parcels in a more robust formula.
We believe these two parcels must be treated together, for reasons explained below.

The fourth parcel in the above formula can be interpreted as a projection on the line
defined by $ \vec d $;
the fifth and last parcel can be interpreted as a projection on the line defined by $ \vec w $.
Returning to the intuitive argument of keeping $ \vec u $ \hglue 0.6pt fixed and letting
$ \vec v $ \hglue 0.6pt converge to $ \vec u $, that is, letting $ \vec d $ \hglue 0.6pt
converge to zero, we see that the direction of $ \vec d $ \hglue 0.6pt may not converge.
Consequently, the direction of $ \vec w = \vec u \times \vec v = \vec u \times \vec d $ \hglue 0.6pt
may not converge either (note that $ \vec w $ \hglue 0.6pt goes to zero).
This suggests that the fourth and fifth parcels must be treated together;
each one individually may not converge.

Let us recall some basic facts about projections.
If $ \vec a $ \hglue 0.6pt is a vector of norm $1$, the matrix $ \vec a \otimes \vec a $
\hglue 0.6pt defined by $ \vec x \mapsto (\vec x \cdot\vec a)\,\vec a $ \hglue 0.6pt represents
a projection on the direction of $ \vec a $.
If $ \vec a $ \hglue 0.6pt and $ \vec b $ \hglue 0.6pt are two orthonormal vectors,
the matrix $ \vec a \otimes \vec a + \vec b \otimes \vec b $ \hglue 0.6pt defined by
$ \vec x \mapsto (\vec x \cdot\vec a)\,\vec a + (\vec x \cdot\vec b)\,\vec b $ \hglue 0.6pt
represents a projection on the plane defined by $ \vec a $ \hglue 0.6pt and $ \vec b $.
If $ \vec a $, $ \vec b $ \hglue 0.6pt and $ \vec c $ \hglue 0.6pt are three orthonormal vectors
in $ \mathbb{R}^3 $, the matrix $ \vec a \otimes \vec a + \vec b \otimes \vec b + \vec c \otimes \vec c $
\hglue 0.6pt is the $ 3 \times 3 $ identity matrix.

Unfortunately, the vectors $ \vec u $, $ \vec d $ \hglue 0.6pt and $ \vec w $ \hglue 0.6pt
are not mutually orthogonal, so we cannot apply the above considerations to them.
However, $ \vec d $ \hglue 0.6pt and $ \vec w $ \hglue 0.6pt are orthogonal so
$ \displaystyle \frac {\vec d}{\|\vec d\,\|}
\otimes \frac {\vec d}{\|\vec d\,\|} + \frac {\vec w}{\|\vec w\,\|} \otimes
\frac {\vec w}{\|\vec w\,\|} $ represents a projection on the plane defined by $ \vec d $ \hglue 0.6pt
and $ \vec w $ \hglue 0.6pt which we may denote by $Q$.
This plane is not orthogonal to $ \vec u $ \hglue 0.6pt but is asymptotically orthogonal, that is,
when $ \vec d $ \hglue 0.6pt goes to zero the plane converges to the orthogonal plane to $ \vec u $.
We see that the last parcel in the above formula for $ R \vec x $ \hglue 0.6pt is exactly
$ \displaystyle \biggl( \frac {\vec w}{\|\vec w\,\|} \otimes \frac {\vec w}{\|\vec w\,\|}
\biggr)\, \vec x $.
% The fourth parcel is not exactly a projection because
% \[ \|\vec d\,\| = 2\, \sin\,\frac \theta 2 = \sqrt{2-2c} = \sqrt {\frac 2 {1+c}}\;s \]
% Thus,
% \[ \frac {1+c}2\, \frac 1{s^2}\, \cdot \vec d\otimes \vec d
% + \frac 1{s^2}\, \vec w\otimes \vec w = \mbox{proj}_Q  \]

Since $Q$ is not exactly orthogonal to $ \vec u $,
we prefer to introduce a new vector $ \vec d^\star = d - (\vec d\cdot \vec u)\, \vec u $
which is orthogonal to both $ \vec u $ \hglue 0.6pt and $ \vec w $.
It is easy to check that $ \|\vec d^\star\| = s $, thus
\[ \frac 1{s^2}\, \vec d^\star\otimes \vec d^\star + \frac 1{s^2}\, \vec w\otimes \vec w
= \mbox{proj}_{\vec u^\perp} = I_{3\times 3} - \mbox{proj}_{\vec u}
= I_{3\times 3} - \vec u \otimes \vec u \]

We conclude that 
\[ R\vec x = \frac {2c}{1+c}\,(\vec x\cdot \vec u)\, \vec u
- \frac 1{1+c}\, (\vec x \cdot \vec d)\, \vec u
+ \frac{1+2c}{1+c}\, (\vec x \cdot \vec u)\, \vec d
+ \frac c{s^2}\, (\vec x \cdot \vec d)\, \vec d
+ \vec x - (\vec x\cdot\vec u)\, \vec u
- \frac 1{s^2}\, (\vec x \cdot \vec d^\star)\, \vec d^\star = {} \]
\[ {} = \vec x
- \frac {1-c}{1+c}\, (\vec x\cdot\vec u)\, \vec u
- \frac 1{1+c}\, (\vec x \cdot \vec d)\, \vec u
+ \frac{1+2c}{1+c}\, (\vec x \cdot \vec u)\, \vec d
+ \frac c{s^2}\, (\vec x \cdot \vec d)\, \vec d
- \frac 1{s^2}\, (\vec x \cdot \vec d^\star)\, \vec d^\star = {}\]
\[ {} = \vec x
- \frac {2\,(1-c)}{1+c}\, (\vec x\cdot\vec u)\, \vec u
- \frac 2{1+c}\, (\vec x \cdot \vec d)\, \vec u
+ \frac {2c}{1+c}\, (\vec x \cdot \vec u)\, \vec d
- \frac 1{1+c}\, (\vec x \cdot \vec d)\, \vec d \]

% Just to make sure, let us check that $ R \vec u = \vec v $ and $ R \vec v \cdot \vec v = c $\,:
% \[ (1+c)\, R \vec u = (1+c)\, \vec u - 2\,(1-c)\, \vec u + 2\, (1-c)\,\vec u
%  + 2c\, \vec d + (1-c)\, \vec d = \]
% \vskip -8mm
% \[ = (1+c)\,\vec u + (1+c)\,\vec d = (1+c)\,\vec v \mbox{\hbox to 21mm{\hfil}} \]
% \[ (1+c)\, R\vec v = (1+c)\,\vec v - 2c\,(1-c)\,\vec u -2\,(1-c)\,\vec u + 2c^2\,\vec d - (1-c)\,\vec d
% = \]
% \vskip -6.5mm
% \[ = (1+c)\,\vec v -2\,(1-c)\,(1+c)\,\vec u + (2c^2+c-1)\,\vec d \]
% \[ R\vec v = \vec v -2\,(1-c)\,\vec u + (2c-1)\,(\vec v-\vec u) = 2c\,\vec v - \vec u \]
% \[ R\vec v \cdot \vec v = 2c -c = c \]

The above formula is convenient for several reasons.
First, there are no divisions by small quantities, namely by $ s^2 $.
Second, we don't need to compute $ \vec w $ (through an exterior product) or its norm $s$.
Third, this expression clearly shows that $R$ is a small perturbation of the identity matrix.
We can even distinguish between first order corrections (the third and fourth parcels)
and second order corrections (the second and fifth/last parcels).

We may re-write the above expression for $R$ in terms of $ \vec u $ \hglue 0.6pt and $ \vec v $\,:
\[ R\vec x = \vec x
 - \frac 1{1+c}\, (\vec x\cdot\vec u)\,\vec u
 - \frac 1{1+c}\, (\vec x\cdot\vec v)\,\vec u
 + \frac {1+2c}{1+c}\, (\vec x\cdot\vec u)\,\vec v
 - \frac 1{1+c}\, (\vec x\cdot\vec v)\,\vec v = {}  \]
\[ {} = \vec x + 2\, (\vec x\cdot\vec u)\,\vec v - \frac 1{1+c}\,
\bigl( (\vec x\cdot\vec u)\,\vec u + (\vec x\cdot\vec v)\,\vec u +
(\vec x\cdot\vec u)\,\vec v + (\vec x\cdot\vec v)\,\vec v\, \bigr) = {} \]
\[ {} = \vec x + 2\, (\vec x\cdot\vec u)\,\vec v - \frac 1{1+c}\,
\bigl(\vec x\cdot(\vec u+\vec v)\bigr)\,(\vec u+\vec v)
= \vec x + 2\, (\vec x\cdot\vec u)\,\vec v - 2\,
\biggl(\vec x\cdot\frac{\vec u+\vec v}{\|\vec u+\vec v\mbox{\hglue 0.5pt}\|}\biggr)\,
\frac{\vec u+\vec v}{\|\vec u+\vec v\mbox{\hglue 0.5pt}\|}                          \]

We can also see $R$ as a composition between two symmetries with respect to an axis
($ 180^\circ $ rotations in $ {\mathbb R}^3 $)\,: a first symmetry with respect to the direction
of $ \vec u + \vec v $ \hglue 0.6pt followed by a second one with respect to the direction of $ \vec v $.

Any of these expressions of $R$ is certainly better than the one obtained in paragraph
\ref{\numb section 8.\numb parag 3} if the angle of rotation is small.
They may be more convenient even if the angle of rotation is not small.

If the angle of rotation is close to $ 180^\circ $ neither of these expressions
(obtained in paragraph \ref{\numb section 8.\numb parag 3} or in the present one) is convenient.
This case is unstable and should be avoided.

