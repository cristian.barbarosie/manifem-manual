
// example presented in paragraph 1.4 of the manual
// http://manifem.rd.ciencias.ulisboa.pt/manual-manifem.pdf
// builds a very curved L-shaped mesh, frontal mesh generation

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz[0], y = xyz[1], z = xyz[2];

	double seg_size = 0.1;

	Manifold sphere = RR3 .implicit ( x*x + y*y + z*z == 1. );

	Cell A ( tag::vertex, tag::of_coords, { -0.866,  0.   , -0.5 } );
	Cell C ( tag::vertex, tag::of_coords, {  0.   , -1.   ,  0.  } );
	Cell E ( tag::vertex, tag::of_coords, {  0.   , -0.866,  0.5 } );
	Cell F ( tag::vertex, tag::of_coords, { -0.866,  0.   ,  0.5 } );
	Cell G ( tag::vertex, tag::of_coords, {  0.866,  0.   , -0.5 } );
	Cell H ( tag::vertex, tag::of_coords, {  1.   ,  0.   ,  0.  } );

	sphere .implicit ( z == -0.5 );
	Mesh AG ( tag::frontal, tag::start_at, A, tag::towards, {0.,-1.,0.},
	                        tag::stop_at,  G, tag::desired_length, seg_size );

	sphere .implicit ( z == 0. );
	Mesh HC ( tag::frontal, tag::start_at, H, tag::stop_at, C,
	          tag::shortest_path, tag::desired_length, seg_size );

 	sphere .implicit ( z == 0.5 );
	Mesh EF ( tag::frontal, tag::start_at, E, tag::stop_at, F,
	          tag::shortest_path, tag::desired_length, seg_size );

	sphere .implicit ( y == 0. );
	Mesh FA ( tag::frontal, tag::start_at, F, tag::stop_at, A,
	          tag::shortest_path, tag::desired_length, seg_size );
	Mesh GH ( tag::frontal, tag::start_at, G, tag::stop_at, H,
	          tag::shortest_path, tag::desired_length, seg_size );

	sphere .implicit ( x == 0. );
	Mesh CE ( tag::frontal, tag::start_at, C, tag::stop_at, E,
	          tag::shortest_path, tag::desired_length, seg_size );

	Mesh polygon ( tag::join, { AG, GH, HC, CE, EF, FA } );

	sphere .set_as_working_manifold ();

	Mesh L_shaped ( tag::frontal, tag::boundary, polygon, tag::desired_length, seg_size );

	L_shaped .export_to_file ( tag::msh, "L-shaped.msh");
	
	std::cout << "produced file L-shaped.msh" << std::endl;

}  // end of main
