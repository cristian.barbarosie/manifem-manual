
// example presented in paragraph 1.4 of the manual
// http://manifem.rd.ciencias.ulisboa.pt/manual-manifem.pdf
// builds an L-shaped mesh on the sphere, controls the shape of side AG

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz[0], y = xyz[1], z = xyz[2];

	Manifold sphere = RR3 .implicit ( x*x + y*y + z*z == 4. );

	Cell A ( tag::vertex, tag::of_coords, { -1., 0. , 2. }, tag::project );
	Cell B ( tag::vertex, tag::of_coords, {  0., 0. , 2. } );  // no need to project
	Cell C ( tag::vertex, tag::of_coords, {  0., 0.5, 2. }, tag::project );
	Cell D ( tag::vertex, tag::of_coords, { -1., 0.5, 2. }, tag::project );
	Cell E ( tag::vertex, tag::of_coords, {  0., 1. , 2. }, tag::project );
	Cell F ( tag::vertex, tag::of_coords, { -1., 1. , 2. }, tag::project );
	Cell G ( tag::vertex, tag::of_coords, {  1., 0. , 2. }, tag::project );
	Cell H ( tag::vertex, tag::of_coords, {  1., 0.5, 2. }, tag::project );

	Manifold snake = sphere .implicit ( y == 0.1 * sin ( 3.14159 * x ) );
	Mesh AB ( tag::segment, A .reverse(), B, tag::divided_in, 10 );
	Mesh BG ( tag::segment, B .reverse(), G, tag::divided_in, 12 );

	sphere .set_as_working_manifold ();
	Mesh BC ( tag::segment, B .reverse(), C, tag::divided_in, 8 );
	Mesh CD ( tag::segment, C .reverse(), D, tag::divided_in, 10 );
	Mesh DA ( tag::segment, D .reverse(), A, tag::divided_in, 8 );
	Mesh CE ( tag::segment, C .reverse(), E, tag::divided_in, 7 );
	Mesh EF ( tag::segment, E .reverse(), F, tag::divided_in, 10 );
	Mesh FD ( tag::segment, F .reverse(), D, tag::divided_in, 7 );
	Mesh GH ( tag::segment, G .reverse(), H, tag::divided_in, 8 );
	Mesh HC ( tag::segment, H .reverse(), C, tag::divided_in, 12 );

	Mesh ABCD ( tag::rectangle, AB, BC, CD, DA );
	Mesh CEFD ( tag::rectangle, CE, EF, FD, CD .reverse() );
	Mesh BGHC ( tag::rectangle, GH, HC, BC .reverse(), BG );
	Mesh L_shaped ( tag::join, ABCD, CEFD, BGHC );

	L_shaped .export_to_file ( tag::msh, "L-shaped.msh");
	
	std::cout << "produced file L-shaped.msh" << std::endl;

}  // end of main
