
// example presented in paragraph 1.4 of the manual
// http://manifem.rd.ciencias.ulisboa.pt/manual-manifem.pdf
// builds a torus with variable segment size

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz[0], y = xyz[1], z = xyz[2];

	Function seg_size = 0.15 + 0.09 * x;

	Function f1 = x*x + y*y;
	Function f2 = 1. - power ( f1, -0.5 );
	Function d = f1 * f2 * f2 + z*z;  // squared distance to a circle in the xy plane

	RR3 .implicit ( d == 0.15 );
	Mesh torus ( tag::frontal, tag::desired_length, seg_size );
	torus .export_to_file ( tag::msh, "torus.msh");

	RR3 .set_as_working_manifold();
	Mesh full_torus ( tag::frontal, tag::boundary, torus, tag::desired_length, seg_size );
	full_torus .export_to_file ( tag::msh, "full-torus.msh");
	
	std::cout << "produced files torus.msh and full-torus.msh" << std::endl;

}  // end of main

