
# %.dvi : %.tex
# 	latex $<

manual.dvi : manual.tex manual-cap-??.tex index.tex changelog.tex
	latex manual

view-dvi : manual.dvi
#	xdvi -paper 31cmx22cm $^ &
	xdvi -s 6 $^ &

dvi : manual.dvi

# $< is equivalent to $^ ?
%.ps : %.dvi
#	dvips -t landscape -o $@ $<
	dvips -o $@ $<

ps : manual.ps

view-ps : manual.ps
#	gv $^ &
	/cygdrive/c/Users/cristian/AppData/Local/Apps/Evince-2.32.0/bin/evince.exe $^ &

%.pdf: %.ps
	ps2pdf $^

#%.pdf: %.dvi
#	dvipdfm $^

pdf : manual.pdf

view-pdf: manual.pdf
	cygstart $^

.PHONY: view-ps, view-pdf, view-dvi, ps, pdf, dvi

