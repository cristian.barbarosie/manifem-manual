
          %--------------------------------------%
\chapter{~~Functions and variational formulations}\label{\numb section 6}
          %--------------------------------------%

In \maniFEM, finite elements (or a mere integration of a function)
only work if the compilation flag {\small\tt MANIFEM\_NO\_FEM} is de-activated
and the {\small\tt Eigen} library is installed;
paragraph \ref{\numb section 12.\numb parag 15} provides more details.


          %---------%
\section{~~Functions}\label{\numb section 6.\numb parag 1}
          %---------%

As the reader may have already noted, all examples in {\maniFEM} begin by declaring a
Euclidian {\small\tt\verm{Manifold}} and then go on by building a coordinate system\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,
   baselinestretch=0.94,framesep=2mm                     ]
   \verm{Manifold} RR2 ( \taag::Euclid, \taag::of_dim, \laranja{2} );
   \verm{Function} \azul{xy} = RR2 .build_coordinate_system ( \taag::Lagrange, \taag::of_degree, \laranja{1} );
   \verm{Function} \azul{x} = xy[\laranja{0}], \azul{y} = xy[\laranja{1}];
\end{Verbatim}

Paragraph \ref{\numb section 12.\numb parag 2} explains the coloring conventions observed
in this manual for {\tt C++} code.
Paragraph \ref{\numb section 12.\numb parag 3} provides details about
{\small\tt\taag}\hglue0.5pt s.

In the above, {\small\tt\verm{xy}} is a vector-valued {\small\tt\verm{Function}}
with two components, {\small\tt x} and {\small\tt y}.
The method {\small\tt\verm{Manifold}::build\_\,coordinate\_\,system} invokes, invisibly to the user,
statements similar to
\footnote{{} Such a statement appears explicitly in paragraph \ref{\numb section 7.\numb parag 29}.}

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,
   baselinestretch=0.94,framesep=2mm                     ]
   \verm{Function} \azul{xy} ( \taag::lives_on, \taag::vertices, \taag::has_size, \laranja{2} );
\end{Verbatim}

The above declaration of {\small\tt\azul{xy}} starts a complex process; behind the curtains,
{\maniFEM} builds a {\small\tt Field} object associated to {\small\tt xy}.\,%
\footnote{{} In order to avoid having too many names exposed in {\footnotesize\tt namespace}
{\footnotesize\tt\verm{maniFEM}}, the name {\footnotesize\tt Field} is hidden inside the
namespace {\footnotesize\tt \taag::Util}.}
The {\small\tt Field} object changes the behaviour of {\maniFEM} in what regards
initialization of {\small\tt\verm{Cell}}\hglue0.5pt s.
Since {\small\tt xy} is of type Lagrange of degree $1$ and the geometric dimension is $2$,
each newly built vertex {\small\tt\verm{Cell}} will have memory space reserved
for two {\small\tt double} precision numbers.
If {\small\tt A} is a vertex {\small\tt\verm{Cell}}, an assignment like
{\small\tt x(A)} {\small\tt  =} {\small\tt\laranja{1.5}} sets the value
of the first component of the {\small\tt Field} associated to {\small\tt xy} at {\small\tt A}.
Note that negative vertices will have no coordinates, only positive vertices will.

If we declare {\small\tt xy} to be of type Lagrange of degree 2, not only future vertices will
have space reserved for two {\small\tt double}\hglue0.5pt s, but also future segments.
So, we may assign the value of {\small\tt y} associated to a segment {\small\tt AB} by using
the syntax {\small\tt y(AB)} {\small\tt =} {\small\tt\laranja{0.75}}.
This number may be interpreted as the value taken by {\small\tt y} at the middle of
segment {\small\tt AB}.
This feature is not yet implemented.

Other {\small\tt\verm{Function}}\hglue0.5pt s are not related with space in the computer's memory
(they have no subjacent {\small\tt Field}).
For instance, there are arithmetic expressions like 

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,baselinestretch=0.94]
   \verm{Function} \azul{norm} = \verm{power} ( x*x + y*y + z*z, \laranja{0.5} );
\end{Verbatim}

We can still access the value of such a {\small\tt\verm{Function}} at a cell like in
{\small\tt double} {\small\tt\azul{n}} {\small\tt =} {\small\tt norm(A)};
the computer will return the value of the corresponding arithmetic expression,
replacing the symbols {\small\tt x}, {\small\tt y} and {\small\tt z} by the corresponding
values at cell {\small\tt A}.
But it makes no sense to change the value of {\small\tt norm} at cell {\small\tt A}.
Thus, statements like {\small\tt x(A)} {\small\tt =} {\small\tt\laranja{1.}}$\;$work fine,
while {\small\tt norm(A)} {\small\tt =} {\small\tt\laranja{1.}}$\;$produces a run-time error.

There are also abstract basis {\small\tt\verm{Function}}\hglue0.5pt s which are mere placeholders,
used for implementing {\small\tt\verm{VariationalFormulation}}\hglue0.5pt s,
as explained in paragraph \ref{\numb section 6.\numb parag 3}, and also
for hand-coded {\small\tt\verm{FiniteElement}}\hglue0.5pt s,
as explained in paragraphs \ref{\numb section 11.\numb parag 11} and
\ref{\numb section 11.\numb parag 12}.

The {\small\tt deriv} method performs symbolic differentiation\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,
   baselinestretch=0.94,framesep=2mm                     ]
   \verm{Function} \azul{d_norm_d_x} = norm .deriv (x);
   \verm{Function} \azul{d_norm_d_y} = norm .deriv (y);
\end{Verbatim}


          %---------------------%
\section{~~Integrating functions}\label{\numb section 6.\numb parag 2}
          %---------------------%

Differentiating symbolic expressions is a relatively easy task.
Symbolically computing primitives is quite a different story.
\ManiFEM{} does not compute symbolic primitives nor performes symbolic integration.

Rather, \maniFEM{} can integrate functions numerically by using Gauss quadrature.
Example below computes integrals of functions defined on a spherical mesh.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 6.\numb parag 2}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Manifold} \azul{RR3} ( \taag::Euclid, \taag::of_dim, \laranja{3} );
   \verm{Function} \azul{xyz} = RR3 .build_coordinate_system ( \taag::Lagrange, \taag::of_degree, \laranja{1} );
   \verm{Function} \azul{x} = xyz[\laranja{0}], \azul{y} = xyz[\laranja{1}], \azul{z} = xyz[\laranja{2}];

   \verm{Manifold} \azul{sphere_manif} = RR3.implicit ( x*x + y*y + z*z == \laranja{1.} );
   \verm{Mesh} \azul{sphere} = \verm{Mesh}::Build ( \taag::frontal )
      .entire_manifold() .desired_length ( \laranja{0.1} );

   \verm{Integrator} \azul{integr} ( \taag::Gauss, \taag::tri_6 );

   std::cout << \verde{"integral of 1 = "} << integr ( \laranja{1.}, \taag::on, sphere ) << std::endl;
   std::cout << \verde{"integral of x = "} << integr ( x, \taag::on, sphere ) << std::endl;
   std::cout << \verde{"integral of x*x = "} << integr ( x*x, \taag::on, sphere ) << std::endl;
   std::cout << \verde{"integral of x*y = "} << integr ( x*y, \taag::on, sphere ) << std::endl;
\end{Verbatim}

Invisibly to the user, {\maniFEM} builds a {\small\tt\verm{FiniteElement}} compatible with the
inner structure of the coordinate functions {\small\tt xyz}.
The quadrature is performed on the master element rather than on the physical cells.
Section \ref{\numb section 11} describes \maniFEM's approach to finite elements.
Paragraph \ref{\numb section 12.\numb parag 5} describes the lifecycle of a
{\small\tt\verm{FiniteElement}} -- {\small\tt\verm{Integrator}} pair.


          %------------------------%
\section{~~Variational formulations}\label{\numb section 6.\numb parag 3}
          %------------------------%

In this section, we define a variational formulation (corresponding to a Poisson problem)
by declaring a bilinear form and a linear form.
These forms involve an unknown {\tt\small\verm{Function}} {\tt\small temperature}
and a test {\tt\small\verm{Function}} {\tt\small phi};
both are mere symbols, they hold no data.
Meaningful data will be produced later, upon invocation of the method
{\small\tt \verm{VariationalFormulation}::solve}.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 6.\numb parag 3}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh} \azul{bdry} ( ... );
   \verm{Mesh} \azul{domain} ( ... );

   \cinza{// we solve the elliptic equation}
   \cinza{//   - laplacian u = -4}
   \cinza{// whose exact solution is  x^2+y^2}

   \verm{Function} \azul{temperature}
      ( \taag::unknown, \taag::Lagrange, \taag::of_degree, \laranja{1}, \taag::mere_symbol );
   \verm{Function} \azul{phi} ( \taag::test, \taag::Lagrange, \taag::of_degree, \laranja{1}, \taag::mere_symbol );

   \verm{VariationalFormulation} \azul{Laplace} =
         (   temperature .deriv(x) * phi .deriv(x) \cinza{// +}
           + temperature .deriv(y) * phi .deriv(y) ) .integral_on ( domain )
      == \laranja{-4.} * phi .integral_on ( domain );

   Laplace .boundary_condition
      ( \taag::Dirichlet, temperature == x*x + y*y, \taag::on, bdry );
\end{Verbatim}

Unlike the action of an {\small\tt\verm{Integrator}}, shown in paragraphs
\ref{\numb section 1.\numb parag 1} and \ref{\numb section 6.\numb parag 2},
the method {\small\tt\verm{Function}::} {\small\tt ::integral\_\,on} returns a delayed integral.
Such an integral will only be numerically computed later, after docking of a
finite element on a cell, replacing the symbolic unknown by one basis function and
replacing the symbolic test function {\small\tt phi} by another basis function in
that finite element.
Likewise, the derivatives of symbolic unknown and test functions produce delayed derivatives,
evaluated later.

The method {\small\tt \verm{VariationalFormulation}::solve} starts a complex process\,:
it assembles a matrix and a vector defining a system of linear equations,
then calls a method for solving this system.
This process relies on the {\small\tt Eigen} library for storing the matrix and the vector
and for solving the system of linear equations.
The method {\small\tt \verm{VariationalFormulation}::solve}
returns a {\small\tt\verm{Function}} containing the solution of the system;
this {\small\tt\verm{Function}} can be used, for instance, for visualization purposes.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 6.\numb parag 3}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   temperature = Laplace .solve();
   \cinza{// above, we use the same Function wrapper 'temperature'}
   \cinza{// or, equivalently, we can create a new Function wrapper :}
   \cinza{// Function computed_temp = Laplace .solve();}

   \cinza{// for visualizing the results, we use a composite mesh :}
   \verm{Mesh}::Composite \azul{mesh_with_temp} = \verm{Mesh}::Build ( \taag::gather )
      .mesh ( \verde{"triangle"}, domain )
      .function ( \verde{"temperature"}, temperature );
   mesh_with_temp .export_to_file ( \taag::gmsh, \verde{"triangle-Dirichlet.msh"} );
\end{Verbatim}

The same example was briefly presented in paragraph \ref{\numb section 1.\numb parag 10};
see figure \ref{\numb section 1.\numb fig 12}.

The code produced by this approach is limited to Lagrange finite elements of degree $1$ for now.
Section \ref{\numb section 11} shows how to solve PDEs using low-level {\tt C++} constructs;
there, the user can choose e.g.{} Lagrange finite elements of degree $2$.

Also, code produced by this approach can be slow.
The approach described in section \ref{\numb section 11} allows the user to optimize certain
parts of the code, thus increasing the speed.
For instance, the loop over cells of the domain where the matrix is assembled can be used
for assembling simultaneously the vector.
Most notably, the use of hand-coded integrators
(described in paragraphs \ref{\numb section 11.\numb parag 11} and
\ref{\numb section 11.\numb parag 12}) can dramatically decrease the computation time.


          %-----------------------%
\section{~~Integrals on subdomains}\label{\numb section 6.\numb parag 4}
          %-----------------------%

The user may provide, in linear and bilinear forms,
integrals on subdomains and on (parts of) the boundary.
On the boundary, \maniFEM{} will use Lagrange finite elements of lower dimension.

Below, we consider a positive heat source in the left half of the domain, a negative heat source
in the right half of the domain. Also, heat enters through the side {\small\tt FC} and
exits through the side {\small\tt AD}, with a non-constant distribution (of density {\small\tt y}).
The temperature is kept to zero along sides {\small\tt DF} and {\small\tt CA}.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 6.\numb parag 4}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{VariationalFormulation} \azul{Laplace} =
         (   temperature .deriv(x) * phi .deriv(x) \cinza{// +}
           + temperature .deriv(y) * phi .deriv(y) ) .integral_on ( domain )
      ==   phi .integral_on ( ADEB ) \cinza{// -}
         - phi .integral_on ( BEFC ) \cinza{// -}
         - ( y * phi ) .integral_on ( AD ) \cinza{// +}
         + ( y * phi ) .integral_on ( FC );

   Laplace .boundary_condition
      ( \taag::Dirichlet, temperature == \laranja{0.}, \taag::on, Dirichlet_bdry );
\end{Verbatim}

Figure \ref{\numb section 6.\numb fig 1} shows the computed {\small\tt temperature}.

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{C}{\small\tt\textcolor{textindraw}{C}}
  \psfrag{D}{\small\tt\textcolor{textindraw}{D}}
  \psfrag{E}{\small\tt\textcolor{textindraw}{E}}
  \psfrag{F}{\small\tt\textcolor{textindraw}{F}}
  \includegraphics[width=120mm]{rectangle-Poisson}
  \caption{a variational formulation featuring integrals on subdomains}
  \label{\numb section 6.\numb fig 1}
\end{figure}

Also, one can mix triangles and quadrilaterals for solving a variational problem.
\ManiFEM{} will choose P1 Lagrange finite elements on triangles and Q1 Lagrange finite elements
on quadrilaterals.
These finite elements are compatible at the interface between triangles and rectangles.
Paragraph \ref{\numb section 9.\numb parag 3} shows an example.


          %------------------%
\section{~~Concentrated loads}\label{\numb section 6.\numb parag 5}
          %------------------%

A load concentrated at a vertex {\small\tt P} can be added to a
{\small\tt\verm{VariationalFormulation}} as shown below.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 6.\numb parag 5}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{VariationalFormulation} \azul{Laplace} =
         (   temperature .deriv(x) * phi .deriv(x) \cinza{// +}
           + temperature .deriv(y) * phi .deriv(y) ) .integral_on ( domain )
      ==   phi .integral_on ( domain ) \cinza{// -}
         - \laranja{0.5} * phi .value_at (P);
\end{Verbatim}

It would be more elegant to allow for an expression like {\small\tt phi(P)} instead of
{\small\tt phi.value\_\,at(P)}.
However, syntactic constraints in {\tt C++} do not allow for such a construct.


          %-----------%
\section{~~Future work}\label{\numb section 6.\numb parag 6}
          %-----------%

Implement variational formulations for vector-valued PDEs and for PDEs on quotient manifolds.

Variational formulations should use hand-coded finite elements
(paragraphs \ref{\numb section 11.\numb parag 11} and \ref{\numb section 11.\numb parag 12})
whenever these are available.
