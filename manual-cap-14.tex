
            %--------------------------%
\chapter {~~{Frequently asked questions}}\label{\numb section 14}
            %--------------------------%

          %------------------------------------------%
\section{~~How do I compile my own {\tt main} files ?}\label{\numb section 14.\numb parag 1}
          %------------------------------------------%

See paragraph \ref{\numb section 12.\numb parag 15}.


          %-----------------------------------------------------------------%
\section{~~Is {\maniFEM} thread-safe ? Does it support parallel processing ?}
          %-----------------------------------------------------------------%
\label{\numb section 14.\numb parag 2}

No special care has been taken with this regard.

If the problem at hand can be split into several smaller problems,
then it is of course possible to launch several independent processes simultaneously.\,%
\footnote {{} e.g.\ as described in section 5 of the paper\,: C.~Barbarosie, A.M.~Toader,
Optimization of bodies with locally periodic microstructure,
Mechanics of Advanced Materials and Strucures 19, p.\ 290-301, 2012}


          %-----------------------------------------------------%
\section{~~What's the difference between a mesh and a manifold ?}\label{\numb section 14.\numb parag 3}
          %-----------------------------------------------------%

A {\small\tt\verm{Mesh}} is a collection of {\small\tt\verm{Cell}}\hglue0.5pt s of the
same dimension.

A {\small\tt\verm{Manifold}} is a {\tt C++} object modelling an abstract mathematical concept.
Recall that in this manual the term ``manifold'' means a differentiable manifold with no boundary
like the straight line $ \{(x,y)\in\mathbb R^2\!: x+y = 1 \} $ or the paraboloid
$ \{(x,y,z)\in\mathbb R^3\!: z = x^2+y^2 \} $ or the sphere
$ S^2 = \{(x,y,z)\in\mathbb R^3\!: x^2+y^2+z^2 = 1 \} $.
There are Euclidian {\small\tt\verm{Manifold}}\hglue0.5pt s, there are implicitly defined
sub-{\small\tt\verm{Manifold}}\hglue0.5pt s, there are parametric {\small\tt\verm{Manifold}}\hglue0.5pt s
and also quotient {\small\tt\verm{Manifold}}\hglue0.5pt s.

A {\small\tt\verm{Mesh}} may be the discretization of a {\small\tt\verm{Manifold}}
or of a piece (region) of a {\small\tt\verm{Manifold}}.

A {\small\tt\verm{Manifold}} is smooth (unless it has singular points, like in 
paragraphs \ref{\numb section 3.\numb parag 21}, \ref{\numb section 3.\numb parag 22},
\ref{\numb section 7.\numb parag 15}, \ref{\numb section 7.\numb parag 16}),
a {\small\tt\verm{Mesh}} can be thought of as a polygonal surface.

See paragraphs \ref{\numb section 1.\numb parag 1}, \ref{\numb section 2.\numb parag 6},
\ref{\numb section 2.\numb parag 13}, \ref{\numb section 2.\numb parag 19},
\ref{\numb section 2.\numb parag 20} and  \ref{\numb section 8.\numb parag 2};
see section \ref{\numb section 7}.


          %-----------------------------------------------------%
\section{~~What's the difference between methods {\tt glue\_\,on\_\,bdry\_\,of} and
  {\tt add\_\,to\_\,mesh}, or between {\tt cut\_\,from\_\,bdry\_\,of} and
  {\tt remove\_\,from\_\,mesh} ?}
          %-----------------------------------------------------%
\label{\numb section 14.\numb parag 4}


For adding a cell to a mesh which is the boundary of some other cell,
you should use {\small\tt\verm{Cell}::Core::glue\_\,on\_\,bdry\_\,of} and
{\small\tt\verm{Cell}::Core::cut\_\,from\_\,bdry\_\,of}.
Otherwise, you should use {\small\tt\verm{Cell}::Core::add\_\,to\_\,mesh} and
{\small\tt\verm{Cell}::Core::remove\_\,from\_\,mesh}.

In some cases (when you are sure there is no mesh ``above'' that other cell)
it makes no difference, so you can use either.
For instance, in {\small\tt\verm{Cell}} constructors and destructors
we have chosen to use {\small\tt\verm{Cell}::Core::add\_\,to\_\,mesh} and
{\small\tt\verm{Cell}::Core::remove\_\,from\_\,mesh}
because they are slightly faster.

          %----------------------------------------------------------------------%
\section{~~What's the difference between {\tt\taag::join} and {\tt\taag::gather} ?}
          %----------------------------------------------------------------------%
\label{\numb section 14.\numb parag 5}

An object {\small\tt\verm{Mesh}::Build} {\small\tt(} {\small\tt\taag::join} {\small\tt)}
simply glues several meshes (of the same dimension) together.
The result is a large non-differentiated mesh where the different component meshes cannot
be distinguished.

An object {\small\tt\verm{Mesh}::Build} {\small\tt(} {\small\tt\taag::gather} {\small\tt)}
produces a composite mesh which is a heterogeneous collection of meshes and other types
of information (cells, functions).
Component meshes (and other informations) can be retrieved through dedicated methods,
using names (strings).
Component meshes may have different dimensions.
This is useful e.g.\ for keeping the boundary of a mesh (or different zones of the boundary)
or interfaces embedded in the mesh.


          %----------------------------------------------------%
\section{~~What happens if we try to identify a twisted pair of
           opposite faces of a parallelogram ?}
          %----------------------------------------------------%
\label{\numb section 14.\numb parag 6}

If, in paragraph \ref{\numb section 7.\numb parag 19}, we ask {\maniFEM} to identify
{\small\tt BC} with {\small\tt DA} (forgetting to reverse {\small\tt DA}),
{\maniFEM} will complain about orientations.

\ManiFEM{} has been designed around the concept of oriented mesh so it will
bluntly refuse to mesh a non-orientable manifold.
Had it been otherwise, identifying {\small\tt BC} with {\small\tt AD}
would produce a M\"obius strip.

Similarly, in paragraph \ref{\numb section 7.\numb parag 20}, a statement like

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,
   baselinestretch=0.94,framesep=2mm                     ]
   \verm{Mesh} \azul{Klein} = square .fold ( \taag::"identify, AB, \taag::with, CD.reverse(),
                               \taag::identify, BC, \taag::with, DA,
                               \taag::use_existing_vertices                 );
\end{Verbatim}

\noindent would produce a Klein surface if {\maniFEM} were not so stubbornly reliant on
oriented meshes.

On the other hand, the statement

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,
   baselinestretch=0.94,framesep=2mm                     ]
   \verm{Mesh} \azul{projective} = square .fold ( \taag::identify, AB, \taag::with, CD,
                                    \taag::identify, BC, \taag::with, DA,
                                    \taag::use_existing_vertices       );
\end{Verbatim}

\noindent would produce a projective plane if {\maniFEM} were not so reluctant of
non-oriented objects.


          %-------------------------------------------------------------------%
\section{~~Why do we declare coordinates to be of type Lagrange, of degree 1 ?}
          %-------------------------------------------------------------------%
\label{\numb section 14.\numb parag 7}

That's because we want (positive) vertices to have coordinates.
Other cells (e.g.\ segments) will have no coordinates.
See paragraph \ref{\numb section 6.\numb parag 1}.


          %-----------------------------------------------------------%
\section{~~Will {\maniFEM} support parallel processing in the future ?}
          %-----------------------------------------------------------%
\label{\numb section 14.\numb parag 8}

At the moment, this is not part of our plans.
See also paragraph \ref{\numb section 14.\numb parag 2}.

