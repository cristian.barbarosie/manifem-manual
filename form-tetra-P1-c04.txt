
element = FiniteElement("Lagrange", tetrahedron, 1)
v = TestFunction(element)
u = TrialFunction(element)

# case 4
a = ( 77*v.dx(0) * u.dx(0) + 100*v.dx(0) * u.dx(1) + 13*v.dx(0) * u.dx(2) \
      + 17*v.dx(1) * u.dx(0) + 23*v.dx(1) * u.dx(1) + 37*v.dx(1) * u.dx(2) \
      + 41*v.dx(2) * u.dx(0) + 47*v.dx(2) * u.dx(1) + 51*v.dx(2) * u.dx(2) ) * dx
form = compile_form( a )

f = open ("form.h",'w')
for l in form : f.write(l)


#  tetrahedron = PQRS
#  P ( coord[0], coord[1],  coord[2]  )
#  Q ( coord[3], coord[4],  coord[5]  )
#  R ( coord[6], coord[7],  coord[8]  )
#  S ( coord[9], coord[10], coord[11] )

    PS_x = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[9] * FE8_C0_D001_Q1[0][0][1];
    PR_z = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[8] * FE8_C0_D001_Q1[0][0][1];
    PS_z = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[11] * FE8_C0_D001_Q1[0][0][1];
    PR_x = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[6] * FE8_C0_D001_Q1[0][0][1];
    PQ_x = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[3] * FE8_C0_D001_Q1[0][0][1];
    PR_y = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[7] * FE8_C0_D001_Q1[0][0][1];
    PS_y = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[10] * FE8_C0_D001_Q1[0][0][1];
    PQ_z = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[5] * FE8_C0_D001_Q1[0][0][1];
    PQ_y = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[4] * FE8_C0_D001_Q1[0][0][1];
	 
    det_PRSyz = PR_y * PS_z - PS_y * PR_z;
    det_PQRyz = PQ_y * PR_z - PR_y * PQ_z;
    det_PRSxz = PS_x * PR_z - PS_z * PR_x;
    det_PQSxz = PQ_x * PS_z - PQ_z * PS_x;
    det_PQRxz = PR_x * PQ_z - PQ_x * PR_z;
    det_PQSyz = PS_y * PQ_z - PQ_y * PS_z;
    det_PRSxy = PR_x * PS_y - PS_x * PR_y;
    det_PQSxy = PS_x * PQ_y - PQ_x * PS_y;
    det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

    sp[40] = 77 * det_PRSyz * det_PRSyz;
    sp[41] = 77 * det_PQSyz * det_PRSyz;
    sp[42] = 77 * det_PQRyz * det_PRSyz;
    sp[43] = 77 * det_PRSyz * det_PQSyz;
    sp[44] = 77 * det_PQSyz * det_PQSyz;
    sp[45] = 77 * det_PQRyz * det_PQSyz;
    sp[46] = 77 * det_PRSyz * det_PQRyz;
    sp[47] = 77 * det_PQSyz * det_PQRyz;
    sp[48] = 77 * det_PQRyz * det_PQRyz;
	 
    sp[31] = 100 * det_PRSyz * det_PRSxz;
    sp[32] = 100 * det_PQSyz * det_PRSxz;
    sp[33] = 100 * det_PQRyz * det_PRSxz;
    sp[34] = 100 * det_PRSyz * det_PQSxz;
    sp[35] = 100 * det_PQSyz * det_PQSxz;
    sp[36] = 100 * det_PQRyz * det_PQSxz;
    sp[37] = 100 * det_PRSyz * det_PQRxz;
    sp[38] = 100 * det_PQSyz * det_PQRxz;
    sp[39] = 100 * det_PQRyz * det_PQRxz;
	 
    sp[70] = 13 * det_PRSyz * det_PRSxy;
    sp[71] = 13 * det_PQSyz * det_PRSxy;
    sp[72] = 13 * det_PQRyz * det_PRSxy;
    sp[73] = 13 * det_PRSyz * det_PQSxy;
    sp[74] = 13 * det_PQSyz * det_PQSxy;
    sp[75] = 13 * det_PQRyz * det_PQSxy;
    sp[76] = 13 * det_PRSyz * det_PQRxy;
    sp[77] = 13 * det_PQSyz * det_PQRxy;
    sp[78] = 13 * det_PQRyz * det_PQRxy;
	 
    sp[88] = 17 * det_PRSxz * det_PRSyz;
    sp[89] = 17 * det_PQSxz * det_PRSyz;
    sp[90] = 17 * det_PQRxz * det_PRSyz;
    sp[91] = 17 * det_PRSxz * det_PQSyz;
    sp[92] = 17 * det_PQSxz * det_PQSyz;
    sp[93] = 17 * det_PQRxz * det_PQSyz;
    sp[94] = 17 * det_PRSxz * det_PQRyz;
    sp[95] = 17 * det_PQSxz * det_PQRyz;
    sp[96] = 17 * det_PQRxz * det_PQRyz;
	 
    sp[106] = 23 * det_PRSxz * det_PRSxz;
    sp[107] = 23 * det_PQSxz * det_PRSxz;
    sp[108] = 23 * det_PQRxz * det_PRSxz;
    sp[109] = 23 * det_PRSxz * det_PQSxz;
    sp[110] = 23 * det_PQSxz * det_PQSxz;
    sp[111] = 23 * det_PQRxz * det_PQSxz;
    sp[112] = 23 * det_PRSxz * det_PQRxz;
    sp[113] = 23 * det_PQSxz * det_PQRxz;
    sp[114] = 23 * det_PQRxz * det_PQRxz;
	 
    sp[124] = 37 * det_PRSxz * det_PRSxy;
    sp[125] = 37 * det_PQSxz * det_PRSxy;
    sp[126] = 37 * det_PQRxz * det_PRSxy;
    sp[127] = 37 * det_PRSxz * det_PQSxy;
    sp[128] = 37 * det_PQSxz * det_PQSxy;
    sp[129] = 37 * det_PQRxz * det_PQSxy;
    sp[130] = 37 * det_PRSxz * det_PQRxy;
    sp[131] = 37 * det_PQSxz * det_PQRxy;
    sp[132] = 37 * det_PQRxz * det_PQRxy;
	 
    sp[142] = 41 * det_PRSxy * det_PRSyz;
    sp[143] = 41 * det_PQSxy * det_PRSyz;
    sp[144] = 41 * det_PQRxy * det_PRSyz;
    sp[145] = 41 * det_PRSxy * det_PQSyz;
    sp[146] = 41 * det_PQSxy * det_PQSyz;
    sp[147] = 41 * det_PQRxy * det_PQSyz;
    sp[148] = 41 * det_PRSxy * det_PQRyz;
    sp[149] = 41 * det_PQSxy * det_PQRyz;
    sp[150] = 41 * det_PQRxy * det_PQRyz;
    sp[160] = 47 * det_PRSxy * det_PRSxz;
    sp[161] = 47 * det_PQSxy * det_PRSxz;
    sp[162] = 47 * det_PQRxy * det_PRSxz;
    sp[163] = 47 * det_PRSxy * det_PQSxz;
    sp[164] = 47 * det_PQSxy * det_PQSxz;
    sp[165] = 47 * det_PQRxy * det_PQSxz;
    sp[166] = 47 * det_PRSxy * det_PQRxz;
    sp[167] = 47 * det_PQSxy * det_PQRxz;
    sp[168] = 47 * det_PQRxy * det_PQRxz;
    sp[178] = 51 * det_PRSxy * det_PRSxy;
    sp[179] = 51 * det_PQSxy * det_PRSxy;
    sp[180] = 51 * det_PQRxy * det_PRSxy;
    sp[181] = 51 * det_PRSxy * det_PQSxy;
    sp[182] = 51 * det_PQSxy * det_PQSxy;
    sp[183] = 51 * det_PQRxy * det_PQSxy;
    sp[184] = 51 * det_PRSxy * det_PQRxy;
    sp[185] = 51 * det_PQSxy * det_PQRxy;
    sp[186] = 51 * det_PQRxy * det_PQRxy;
    det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
	 assert ( det > 0. );
    det *= 6.;
	 // equivalently, we may divide the second-order determinants by square root of det
	 // but is it faster ?  is computing a square root faster than performing 36 divisions ?
    sp[197] = sp[187] / det;
    sp[198] = sp[188] / det;
    sp[199] = sp[189] / det;
    sp[200] = sp[190] / det;
    sp[201] = sp[191] / det;
    sp[202] = sp[192] / det;
    sp[203] = sp[193] / det;
    sp[204] = sp[194] / det;
    sp[205] = sp[195] / det;

77   40- 48  dx dx
00   31- 39  dx dy
13   70- 78  dx dz
17   88- 96  dy dx
23  106-114  dy dy
37  124-132  dy dz
41  142-150  dz dx
47  160-168  dz dy
51  178-186  dz dz
	 197-205  * vol


    A[0] = sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;   PP
    A[1] = - sp1 - sp2 - sp3;                                     PQ
    A[2] = - sp4 - sp5 - sp6;                                     PR
    A[3] = - sp7 - sp8 - sp9;                                     PS
    A[4] = - sp1 - sp4 - sp7;                                     QP
    A[5] = sp1;                                                   QQ
    A[6] = sp4;                                                   QR
    A[7] = sp7;                                                   QS
    A[8] = - sp2 - sp5 - sp8;                                     RP
    A[9] = sp2;                                                   RQ
    A[10] = sp5;                                                  RR
    A[11] = sp8;                                                  RS
    A[12] = - sp3 - sp6 - sp9;                                    SP
    A[13] = sp3;                                                  SQ
    A[14] = sp6;                                                  SR
    A[15] = sp9;                                                  SS
