
from math import cos, sin, pi

n = 20
step = pi / 2 / n
for i in range (n+1) :
   theta = -pi + i * step
   if  i == 0 :  action = "moveto"
   else :  action = "lineto"
   print (1.5*theta,cos(theta)-2,action)
for i in range (n+1) :
   theta = -pi/2 + i * step
   if  i == 0 :  action = "moveto"
   else :  action = "lineto"
   print (1.5*theta,cos(theta)+sin(theta)-1,action)
for i in range (n+1) :
   theta = i * step
   if  i == 0 :  action = "moveto"
   else :  action = "lineto"
   print (1.5*theta,1-cos(theta)+sin(theta),action)
for i in range (n+1) :
   theta = pi/2 + i * step
   if  i == 0 :  action = "moveto"
   else :  action = "lineto"
   print (1.5*theta,2-cos(theta),action)
   
