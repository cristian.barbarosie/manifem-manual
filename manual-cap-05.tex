
           %----------------%
\chapter {~~Composite meshes}\label{\numb section 5}
           %----------------%


\ManiFEM{} implements the class {\small\tt\verm{Mesh}::Composite} which represents
a mesh together with other informations attached to it.
These informations may be components of the mesh (cells or submeshes), numberings of cells,
functions (e.g.\ solutions of PDEs).
See also paragraph \ref{\numb section 6.\numb parag 3}.


          %-----------------------%
\section{~~Saving composite meshes}\label{\numb section 5.\numb parag 1}
          %-----------------------%

In paragraphs \ref{\numb section 5.\numb parag 1} and \ref{\numb section 5.\numb parag 2}
we show how {\maniFEM} can use the {\small\tt msh} format
(see {\small\tt https://gmsh.info}) for writing and reading back composite meshes.

To fix ideas, we focus on the example in paragraph \ref{\numb section 3.\numb parag 18},
where a mesh called {\small\tt sphere\_\,and\_} {\small\tt\_\,cylinder} is built.
We can save this mesh using method {\small\tt\verm{Mesh}::export\_\,to\_\,file}, then use
{\small\tt gmsh} to draw it.
But if we want to re-use this mesh in another {\maniFEM} program, the declaration

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,
   baselinestretch=0.94,framesep=2mm                     ]
   \verm{Mesh} \azul{sphere_and_cylinder} = \verm{Mesh}::Build ( \taag::import_from_file )
      .format ( \taag::gmsh ) .file_name ( \verde{"filename.msh"} );
\end{Verbatim}

\noindent may be insufficient for our needs.
You see, in the original program (the one presented in paragraph
\ref{\numb section 3.\numb parag 18}) we had at our disposal not only the {\small\tt\verm{Mesh}}
object {\small\tt sphere\_\,and\_\,cylinder} but also other {\tt C++} objects like
{\small\tt piece\_\,of\_\,sph}, {\small\tt piece\_\,of\_\,cyl}, {\small\tt two\_\,circles},
{\small\tt circle\_\,1} and {\small\tt circle\_\,2}.
Depending on our intentions, these objects may be useful.
For instance, we may want to iterate over all segments of {\small\tt circle\_\,1} in order to
impose some boundary condition.
Or, we may want to {\small\tt join} the mesh {\small\tt sphere\_\,and\_\,cylinder} to some
other mesh (a disk for instance).
In order for the {\small\tt join} operation to work well, we must build that other
mesh starting from {\small\tt circle\_\,1} as boundary.
If you don't understand the previous statement, you may want to read again paragraphs
\ref{\numb section 1.\numb parag 4} and \ref{\numb section 3.\numb parag 18}.

The problem is, in the second {\maniFEM} program, the one which imports the mesh
{\small\tt sphere\_\,and\_} {\small\tt\_\,cylinder} from an {\small\tt msh} file
through a {\small\tt\verm{Mesh}::Build}\,er with  {\small\tt\taag::import\_\,from\_\,file},
we do not have access to the {\small\tt circle\_\,1} object.
True, we can recover it by testing each segment of {\small\tt sphere\_\,and\_\,cylinder}
(or rather, both extremities of each segment) against the equation {\small\tt x} {\small\tt ==}
{\small\tt\laranja{1.5}}, but this not elegant.
It would be nice to extract, from one {\small\tt msh} file, several related
{\small\tt\verm{Mesh}} objects.
And this is only possible if we save, in the first {\maniFEM} program, more precise information
than merely one large {\small\tt\verm{Mesh}} object.

Luckily, the {\small\tt msh} file format allows one to specify regions of a mesh
either through {\small\tt EntityTag}\hglue0.5pt s (which are integer numbers)
or through {\small\tt PhysicalName}\hglue0.5pt s (which are strings).
{\ManiFEM} takes advantage of the latter feature to save (and later recover) several
{\small\tt\verm{Mesh}}\hglue0.5pt es, components of one large {\small\tt\verm{Mesh}},
in (and from) one {\small\tt msh} file.

The class {\small\tt\verm{Mesh}::Composite} allows the user to gather, using a clear and elegant syntax,
components of a {\small\tt\verm{Mesh}}, save them to an {\small\tt msh} file and later retrieve them,
using strings as identifiers.
A {\small\tt\verm{Mesh}::Composite} object is simply a collection of heterogeneous informations
(in this paragraph, cells and meshes), endowed with methods for retrieving such components.
It is built using a {\small\tt\verm{Mesh}::Build}\,er with {\small\tt\taag::gather}.
\vfil\eject

Here is how one builds a composite mesh and saves it in an {\small\tt msh} file\,:
\vskip 15pt

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 1}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh}::Composite \azul{sphere_cyl} = \verm{Mesh}::Build ( \taag::gather )
      .cell ( \verde{"start 1"}, start_1 )
      .mesh ( \verde{"circle 1"}, circle_1 )
      .mesh ( \verde{"circle 2"}, circle_2 )
      .mesh ( \verde{"cylinder"}, cyl )
      .mesh ( \verde{"sphere"}, sph );
   sphere_cyl .export_to_file ( \taag::gmsh, \verde{"sphere-cyl-composite.msh"} );
\end{Verbatim}

In the above, we keep the vertex {\small\tt start\_1} because it may be needed for building
another mesh from {\small\tt circle\_1}.
We choose not to keep {\small\tt start\_2} because it is unlikely to be needed for later use.

For geometric dimension higher than $3$, % or for meshes of topological dimension higher than $3$,
the exported file will not be readable by {\small\tt gmsh}.
However, it can still be used by \maniFEM{} to read back {\small\tt\verm{Mesh}} and
{\small\tt\verm{Cell}} objects.


          %---------------------------%
\section{~~Recovering composite meshes}\label{\numb section 5.\numb parag 2}
          %---------------------------%

In a separate program, we retrieve each component of the mesh from the file
created in the previous paragraph,
using methods {\small\tt retrieve\_cell} and {\small\tt retrieve\_mesh}.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 2}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh}::Composite \azul{cyl_sph} = \verm{Mesh}::Build ( \taag::import_from_file )
      .format ( \taag::gmsh ) .file_name ( \verde{"sphere_cyl_composite.msh"} );
   \verm{Cell} \azul{S}        = cyl_sph .retrieve_cell ( \verde{"start 1"} );
   \verm{Mesh} \azul{circle_1} = cyl_sph .retrieve_mesh ( \verde{"circle 1"} );
   \verm{Mesh} \azul{circle_2} = cyl_sph .retrieve_mesh ( \verde{"circle 2"} );
   \verm{Mesh} \azul{cylinder} = cyl_sph .retrieve_mesh ( \verde{"cylinder"} );
   \verm{Mesh} \azul{sphere}   = cyl_sph .retrieve_mesh ( \verde{"sphere"} );
   \verm{Mesh} \azul{all} = \verm{Mesh}::Build ( \taag::join ) .mesh ( cylinder ) .mesh ( sphere );
\end{Verbatim}

Note that, if we read the mesh {\small\tt\azul{cyl\_sph}} from the very same file
but declare it to be of type {\small\tt\verm{Mesh}}
rather than {\small\tt\verm{Mesh}::Composite}, as in
\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,baselinestretch=0.94]
   \verm{Mesh} \azul{cyl_sph} = \verm{Mesh}::Build ( \taag::import_from_file )
      .format ( \taag::gmsh ) .file_name ( \verde{"sphere_cyl_composite.msh"} );
\end{Verbatim}
the {\small\tt\verm{Mesh}::Build}\,er will create one mesh (made of triangles)
from which it is not possible to extract components.


          %--------%
\section{~~Exercise}\label{\numb section 5.\numb parag 3}
          %--------%

Change the code in paragraph \ref{\numb section 3.\numb parag 8}
to save the mesh of tetrahedra as a {\small\tt\verm{Mesh}::Composite}.
In a separate program, read this mesh and extract separately the {\small\tt sphere} and the
{\small\tt ball} and obtain the same results as in paragraph \ref{\numb section 11.\numb parag 9}.

If you encounter difficulties in adapting the code in paragraph
\ref{\numb section 3.\numb parag 8}, see paragraphs \ref{\numb section 5.\numb parag 9}
and \ref{\numb section 5.\numb parag 10} for a self-contained example.


          %----------------------------%
\section{~~Fancy coloring in {\tt gmsh}}\label{\numb section 5.\numb parag 4}
          %----------------------------%

Composite meshes can be used to obtain colorful drawings using {\small\tt gmsh}
(see {\small\tt https://gmsh.info}).
This technique has already been used for the colorful box in paragraph
\ref{\numb section 2.\numb parag 3};
in paragraphs \ref{\numb section 5.\numb parag 4} -- \ref{\numb section 5.\numb parag 6}
we show further examples and provide more details.
\vfil\eject

We use the extrusion technique explained in section \ref{\numb section 4}
to mesh a doughnut surface\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
      label=parag-\ref{\numb section 5.\numb parag 4}.cpp,rulecolor=\color{moldura},
      baselinestretch=0.94,framesep=2mm                                             ]
   RR3 .implicit ( x*x + y*y == \laranja{1.}, z == \laranja{0.} );

   \cinza{// 'big_circle' will follow the above manifold;}
   \cinza{// it can be built either by frontal mesh generation or by joining four segments}

   RR3 .implicit ( (x-\laranja{0.75})*(x-\laranja{0.75}) + z*z == \laranja{0.0625}, y == \laranja{0.} );

   \cinza{// 'small_circle' will follow the above manifold;}
   \cinza{// it can be built either by frontal mesh generation or by joining four segments}

   \cinza{// we must ensure these two meshes have one (and only one) vertex in common}

   RR3 .set_as_working_manifold();
   \verm{Mesh} \azul{torus} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( small_circle ) .slide_along ( big_circle ) .follow_curvature();
\end{Verbatim}

Below we save this doughnut as a composite mesh.
We want to keep the surface {\small\tt torus} and the two circles, for graphics purposes.
% Although we do not need it, we also keep the vertex {\small\tt B};
% due to the requirements of the {\small\tt msh} format, if we keep two meses (here, {\small\tt AB}
% and {\small\tt BC}), we must also keep their intersection.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
      label=parag-\ref{\numb section 5.\numb parag 4}.cpp,rulecolor=\color{moldura},
      baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh}::Composite \azul{comp_msh} = \verm{Mesh}::Build ( \taag::gather )
      .mesh ( \verde{"big loop"}, big_circle )
      .mesh ( \verde{"small loop"}, small_circle )
      .mesh ( \verde{"torus"}, torus );

   comp_msh .export_to_file ( \taag::gmsh, \verde{"composed.msh"} );
\end{Verbatim}

\begin{figure}[ht] \centering
 \includegraphics[width=85mm]{torus-two-loops}
 \caption{a torus with two loops}
 \label{\numb section 5.\numb fig 1}
\end{figure}

We can then choose options in {\small\tt gmsh} like {\small\tt Tools} $\to$ {\small\tt Options} $\to$
{\small\tt Mesh} $\to$ {\small\tt General} $\to$ unselect {\small\tt 2D} {\small\tt element}
{\small\tt edges}, then {\small\tt Aspect} $\to$ {\small\tt Line} {\small\tt width} 5,
then {\small\tt Color} $\to$ {\small\tt Smooth} {\small\tt normals}, then choose colors
{\small\tt By elementary entities}, under {\small\tt One}, {\small\tt Two}, {\small\tt Three}
and so on.
You can find out the correspondence between these numerals and the strings provided to \maniFEM{}
through the {\small\tt add\_cell} and {\small\tt add\_mesh} methods by opening the file
{\small\tt composed.msh} and looking for the {\small\tt PhysicalNames} section.
Or, you can just guess which are the corresponding meshes by trial and error.
For exporting the file in the {\small\tt eps} format, you may also want to disable
{\small\tt Color} $\to$ {\small\tt Use} {\small\tt two-side} {\small\tt lighting},
then go to {\small\tt General} $\to$ {\small\tt Color}
and change the {\small\tt Light} {\small\tt position}, e.g.\ by switching the signs of all coordinates.
You may also want to go to {\small\tt Axes} and disable {\small\tt Show} {\small\tt small}
{\small\tt axes}.
The resulting drawing is shown in figure \ref{\numb section 5.\numb fig 1}.
\vfil\eject


          %------------------------%
\section{~~Another torus with loops}\label{\numb section 5.\numb parag 5}
          %------------------------%

We may prefer to mesh the torus using frontal mesh generation,
in the spirit of paragraphs \ref{\numb section 3.\numb parag 7} and \ref{\numb section 3.\numb parag 24}.
The torus is defined as a submanifold of $ \mathbb{R}^3 $ through the implicit equation
{\small\tt d} {\small\tt ==} {\small\tt\laranja{0.15}}, where

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 5}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Function} \azul{f1} = x*x + y*y;
   \verm{Function} \azul{f2} = \laranja{1.} - \verm{power} ( f1, \laranja{-0.5} );
   \verm{Function} \azul{d} = f1 * f2 * f2 + z*z;  \cinza{// squared distance to a circle in the xy plane}
\end{Verbatim}

The one-dimensional meshes intended to be visible in the drawing (the two loops) must be built first.
Actually, we need to build the torus from four parts, and for this we need to define eight segments.
Only four segments are kept for graphic purposes, joined as {\small\tt big\_loop} and
{\small\tt small\_loop}.
% Again, we must also keep the vertex {\small\tt A}, simply because it is the intersection between
% the two loops.

In the future, the construction described above (torus made of four pieces, from eight segments)
will be simplified by defining new {\small\tt\verm{Mesh}::Build}\,ers with
{\small\tt\taag::frontal} and a call to an {\small\tt embed} method,
which will embed a given interface in the mesh being built.


          %---------------------------%
\section{~~A sphere with three handles}\label{\numb section 5.\numb parag 6}
          %---------------------------%

Here is an example of a more complicated surface with embedded curves
(figure \ref{\numb section 5.\numb fig 2}).

\begin{figure}[ht] \centering
 \includegraphics[width=100mm]{sphere-three-tori}
 \caption{a sphere with three handles}
 \label{\numb section 5.\numb fig 2}
\end{figure}

Comments at the end of paragraph \ref{\numb section 5.\numb parag 5} apply\,:
in the future the user will be able to embed an interface by invoking the
{\small\tt embed} method of the {\small\tt\verm{Mesh}::Build}\,er.


          %--------------------------%
\section{~~Extruding composite meshes}\label{\numb section 5.\numb parag 7}
          %--------------------------%

In this paragraph, we build the same torus as in paragraph \ref{\numb section 5.\numb parag 4},
but declare both {\small\tt swatch} and {\small\tt track} as {\small\tt\verm{Mesh}::Composite}\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 7}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \cinza{// build four arcs of circle NW, WS, SE, EN, then gather them into a big circle}
   \verm{Mesh}::Composite \azul{big_circle} = \verm{Mesh}::Build ( \taag::gather )
      .mesh ( \verde{"NW"}, NW ) .mesh ( \verde{"WS"}, WS ) .mesh ( \verde{"SE"}, SE ) .mesh ( \verde{"EN"}, EN );

   \cinza{// build four arcs of circle AB, BC, CD, DA, then gather them into a small circle}
   \verm{Mesh}::Composite \azul{small_circle} = \verm{Mesh}::Build ( \taag::gather )
      .mesh ( \verde{"AB"}, AB ) .mesh ( \verde{"BC"}, BC ) .mesh ( \verde{"CD"}, CD ) .mesh ( \verde{"DA"}, DA );

   \cinza{// note that A == E, thus these two meshes have one vertex in common}
\end{Verbatim}

It suffices to declare the result of an {\small\tt extrude} operation as a
{\small\tt\verm{Mesh}::Composite} (rather than a mere {\small\tt\verm{Mesh}})
for \maniFEM{} to keep track of component meshes (submeshes).

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 7}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh}::Composite \azul{torus} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( small_circle ) .slide_along ( big_circle ) .follow_curvature();
\end{Verbatim}

In the above, since each {\small\tt circle} has four submeshes, the extruded
(composite) mesh will have sixteen submeshes, stored under names
{\small\tt\verde{"AB}} {\small\tt\verde{times}} {\small\tt\verde{NW"}},
{\small\tt\verde{"BC}} {\small\tt\verde{times}} {\small\tt\verde{SE"}} and so on.
These submeshes can be then recovered as explained in paragraph \ref{\numb section 5.\numb parag 2}.

If we want to retain the two lines in figure \ref{\numb section 5.\numb fig 1},
we must add a cell to each of the composite meshes {\small\tt\azul{small\_\hglue 0.8pt circle}}
and {\small\tt\azul{big\_\hglue 0.8pt circle}}\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 7}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh}::Composite \azul{big_circle} = \verm{Mesh}::Build ( \taag::gather ) .cell ( \verde{"N"}, N )
      .mesh ( \verde{"NW"}, NW ) .mesh ( \verde{"WS"}, WS ) .mesh ( \verde{"SE"}, SE ) .mesh ( \verde{"EN"}, EN );
   \verm{Mesh}::Composite \azul{small_circle} = \verm{Mesh}::Build ( \taag::gather ) .cell ( \verde{"A"}, A )
      .mesh ( \verde{"AB"}, AB ) .mesh ( \verde{"BC"}, BC ) .mesh ( \verde{"CD"}, CD ) .mesh ( \verde{"DA"}, DA );
   \verm{Mesh}::Composite \azul{torus} = \verm{Mesh}::Build ( \taag::extrude )
      .swatch ( small_circle ) .slide_along ( big_circle ) .follow_curvature();
\end{Verbatim}

Then we can recover the two circles on the torus as in

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 7}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh} \azul{big_circle_on_torus} = \verm{Mesh}::Build ( \taag::join )
      .components ( \{ torus .retrieve_mesh ( \verde{"A times NW"} ),
                      torus .retrieve_mesh ( \verde{"A times WS"} ),
                      torus .retrieve_mesh ( \verde{"A times SE"} ),
                      torus .retrieve_mesh ( \verde{"A times EN"} ) \} );
   \cinza{// or declare 'big_circle_on_torus' as Mesh::Composite and use tag::gather}
   \verm{Mesh} \azul{small_circle_on_torus} = \verm{Mesh}::Build ( \taag::join )
      .components ( \{ torus .retrieve_mesh ( \verde{"AB times N"} ),
                      torus .retrieve_mesh ( \verde{"BC times N"} ),
                      torus .retrieve_mesh ( \verde{"CD times N"} ),
                      torus .retrieve_mesh ( \verde{"DA times N"} ) \} );
   \cinza{// or declare 'small_circle_on_torus' as Mesh::Composite and use tag::gather}
\end{Verbatim}


          %-----------------------------%
\section{~~A sequence of extruded meshes}\label{\numb section 5.\numb parag 8}
          %-----------------------------%

When we build a rectangle (or a parallelogram) by extruding two segments like in
paragraphs \ref{\numb section 4.\numb parag 1} and \ref{\numb section 4.\numb parag 2},
we do not have access to all vertices and all sides of the final extruded mesh.
For instance, in paragraph \ref{\numb section 4.\numb parag 2} we have access to vertices
{\small\tt A}, {\small\tt B} and {\small\tt C} but there is a fourth vertex we cannot access.
Likewise, we have access to sides {\small\tt AB} and {\small\tt BC} but not to the other
two sides.

If we want to subsequently join more rectangles in order to produce a more complicated mesh,
we need to use sides of previously built rectangles for building new ones;
otherwise, the {\small\tt join}ing operation will not work correctly.
A similar limitation is discussed in paragraph \ref{\numb section 1.\numb parag 4}.
Extruding composite meshes provides access to geometric components of the extruded meshes.
This paragraph shows how to build extruded meshes in a sequence, using vertices and sides
of previously built rectangles, in order to obtain the mesh shown in figure \ref{\numb section 5.\numb fig 3}.

\begin{figure}[ht] \centering
  \psfrag{A1}{\small\tt\textcolor{textindraw}{A1}}
  \psfrag{A2}{\small\tt\textcolor{textindraw}{A2}}
  \psfrag{A3}{\small\tt\textcolor{textindraw}{A3}}
  \psfrag{B1}{\small\tt\textcolor{textindraw}{B1}}
  \psfrag{B2}{\small\tt\textcolor{textindraw}{B2}}
  \psfrag{B3}{\small\tt\textcolor{textindraw}{B3}}
  \psfrag{C1}{\small\tt\textcolor{textindraw}{C1}}
  \psfrag{C2}{\small\tt\textcolor{textindraw}{C2}}
  \psfrag{C3}{\small\tt\textcolor{textindraw}{C3}}
  \psfrag{D1}{\small\tt\textcolor{textindraw}{D1}}
  \psfrag{D2}{\small\tt\textcolor{textindraw}{D2}}
  \psfrag{D3}{\small\tt\textcolor{textindraw}{D3}}
  \psfrag{E1}{\small\tt\textcolor{textindraw}{E1}}
  \psfrag{E2}{\small\tt\textcolor{textindraw}{E2}}
  \psfrag{E3}{\small\tt\textcolor{textindraw}{E3}}
  \psfrag{F1}{\small\tt\textcolor{textindraw}{F1}}
  \psfrag{F2}{\small\tt\textcolor{textindraw}{F2}}
  \psfrag{F3}{\small\tt\textcolor{textindraw}{F3}}
  \psfrag{G1}{\small\tt\textcolor{textindraw}{G1}}
  \psfrag{G2}{\small\tt\textcolor{textindraw}{G2}}
  \psfrag{G3}{\small\tt\textcolor{textindraw}{G3}}
  \psfrag{H1}{\small\tt\textcolor{textindraw}{H1}}
  \psfrag{H2}{\small\tt\textcolor{textindraw}{H2}}
  \psfrag{H3}{\small\tt\textcolor{textindraw}{H3}}
 \includegraphics[width=130mm]{ribbed-2D}
 \caption{ribbed channel (section)}
 \label{\numb section 5.\numb fig 3}
\end{figure}

Paragraph \ref{\numb section 9.\numb parag 4} shows how to automate the generation of this
sequence of rectangular meshes.


          %-------------------------------------------------------%
\section{~~Writing the numbering of vertices to an {\tt msh} file}
          %-------------------------------------------------------%
\label{\numb section 5.\numb parag 9}

In paragraph \ref{\numb section 5.\numb parag 3}, the numbering of the vertices was built
within the main {\tt C++} program.

There are cases when this is not the best approach.
For instance, we may build a mesh, then build the global matrix obtained from the finite element
discretization of some PDE, then save both the mesh and the matrix to files.
We may want to use later, in another program, the mesh (read from a file) and the matrix
(also read from a file).
For instance, we may want to add some loads in the second program, while taking advantage of the
global matrix (with no loads) computed previously.
To safely do this, we need the numbering of the vertices to be exactly the same
in both programs (this numbering provides the compatibility between the mesh and the matrix).
If we do, like in paragraph \ref{\numb section 5.\numb parag 3}, compute the numbering in the
main function of the second {\tt C++} program, we cannot be 100\% sure that the numbering is the same.
That's because an iterator like
\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,baselinestretch=0.94]
   \verm{Mesh}::Iterator \azul{it} = omega .iterator ( \taag::over_vertices );
\end{Verbatim}
is not guaranteed to run over the vertices of the mesh in a specific order
(there is no canonical ordering of the vertices).
So, we may get a different numbering in the second program.
This is more likely to happen e.g.\ if we run the two programs on different machines.

Paragraphs \ref{\numb section 5.\numb parag 9} and \ref{\numb section 5.\numb parag 10} show
a way to ensure the numbering is exactly the same.
In the present paragraph, we attach the {\small\tt numbering} to the composite mesh
before saving it to a file\,:
\vskip 5mm

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 9}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                             ]
   \verm{Mesh}::Composite \azul{arc} = \verm{Mesh}::Build ( \taag::gather )
      .mesh ( \verde{"arc"}, omega )
      .mesh ( \verde{"top"}, EF )
      .numbering ( numbering );
   arc .export_to_file ( \taag::gmsh, \verde{"arc.msh"} );
\end{Verbatim}

Of course, this same {\small\tt numbering} must be used for numbering the lines and rows
of the matrix obtained from the finite element computations.

% The code in this paragraph uses advanced features of \maniFEM, explained in
% section \ref{\numb section 11}.
% Also, it relies on the {\small\tt Eigen} library; see paragraphs
% \ref{\numb section 12.\numb parag 14} and \ref{\numb section 12.\numb parag 15}
% for details.


          %-------------------------------------------------------%
\section{~~Reading the numbering of vertices from an {\tt msh} file}
          %-------------------------------------------------------%
\label{\numb section 5.\numb parag 10}

In this paragraph we read the numbering from the {\small\tt msh} file created in paragraph
\ref{\numb section 5.\numb parag 9}.
We do this by importing the mesh from the file and, simultaneously, reading the numbering of the vertices :
\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 5.\numb parag 10}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
   \verm{Mesh}::Composite \azul{arc} = \verm{Mesh}::Build ( \taag::import_from_file )
      .format ( \taag::gmsh ) .file_name ( \verde{"arc.msh"} ) .import_numbering();
   \verm{Mesh} \azul{omega} = arc .retrieve_mesh ( \verde{"arc"} );
   \verm{Mesh} \azul{EF}    = arc .retrieve_mesh ( \verde{"top"} );
   \verm{Cell}::Numbering & \azul{numbering} = arc .retrieve_numbering();
\end{Verbatim}

For those readers who are already acquainted with concepts presented
in section \ref{\numb section 11}, we mention that the
{\small\tt numbering} in paragraph \ref{\numb section 5.\numb parag 9} is of type
{\small\tt\verm{Cell}::Numbering::Map}; it was built so.
In the present paragraph, we read back the same {\small\tt numbering} but it is built,
invisibly to the user, in the {\small\tt\verm{Mesh}::Composite} constructor, as a
{\small\tt\verm{Cell}::Numbering::Field}.

A different solution is also possible, involving three steps.
In a first program, we build the mesh and save it to an {\small\tt msh} file,
as a composite mesh.
We do not bother with any numbering at this step;
simply by creating an {\small\tt msh} file, an internal numbering of the vertices
is built, invisibly to the user (the {\small\tt msh} format mandates so).
In a second program, we read back the mesh from the previously created file,
invoking method {\small\tt import\_\hglue 0.5pt numbering} and then
retrieving the {\small\tt numbering} from the composite mesh, as shown above.
We compute the finite element matrix (using the {\small\tt numbering}) and save it to a separate file.
In a third program, we read again the mesh and the numbering from the {\small\tt msh} file,
then read the matrix from the separate data file, then add boundary conditions, then solve the PDE.


