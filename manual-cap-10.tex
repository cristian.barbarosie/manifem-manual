
          %---------%
\chapter{~~Remeshing}\label{\numb section 10}
          %---------%

This section is devoted to remeshing algorithms.
Before proceeding please make sure you have read and understood paragraphs
\ref{\numb section 9.\numb parag 8} and \ref{\numb section 9.\numb parag 9}.

We have already been seen some low-level manipulations of meshes in this manual\,:
paragraph \ref{\numb section 2.\numb parag 21} mentions the possibility of adding
a segment cell to a mesh in order to close a circle;
in paragraphs \ref{\numb section 7.\numb parag 6}\,--\,\ref{\numb section 7.\numb parag 10}
four squares have been eliminated from a mesh.

          %---------------------------------%
\section{~~Cutting a square along a diagonal}\label{\numb section 10.\numb parag 1}
          %---------------------------------%

Suppose we have a mesh of squares and for some reason we want to transform one of the
squares in two triangles, cutting it along one of its diagonals.

One way to achieve this is

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 10.\numb parag 1}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
   ABCD .remove_from ( msh );
   \verm{Cell} \azul{AC} ( \taag::segment, A .reverse(), C );
   \verm{Cell} \azul{ABC} ( \taag::triangle, AB, BC, AC .reverse() );
   \verm{Cell} \azul{CDA} ( \taag::triangle, CD, DA, AC );
   ABC .add_to ( msh );
   CDA .add_to ( msh );
\end{Verbatim}

\begin{figure}[ht] \centering
  \psfrag{A}{\small\tt\textcolor{textindraw}{A}}
  \psfrag{B}{\small\tt\textcolor{textindraw}{B}}
  \psfrag{C}{\small\tt\textcolor{textindraw}{C}}
  \psfrag{D}{\small\tt\textcolor{textindraw}{D}}
  \includegraphics[width=60mm]{malha-quadr}
  \caption{a (part of a) mesh of squares}
  \label{\numb section 10.\numb fig 1}
\end{figure}

The only delicate point here is that the core of the {\small\tt\verm{Cell}} object
{\small\tt ABCD}
will become unused (and inaccessible when the wrapper goes out of syntactic scope).
If you have the garbage collector on (see paragraph \ref{\numb section 12.\numb parag 5}),
the core will be destroyed when the wrapper goes out of scope.
Otherwise, the core will continue to occupy space in the computer's memory giving rise
to an undesirable phenomenon known as ``memory leak''.
If your program does this a lot, it will occupy increasingly more memory without reason.


           %-------------------------------------%
\section{~~{Transforming a square into a triangle}}\label{\numb section 10.\numb parag 2}
           %-------------------------------------%

Instead of discarding the square and building two new triangles, we can use the same
{\small\tt\verm{Cell}} object {\small\tt ABCD}
by transforming it into a triangle, without removing it from {\small\tt msh}, then add
a new triangle to obtain the same result as in paragraph \ref{\numb section 10.\numb parag 1}.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 10.\numb parag 2}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
   CD .cut_from_bdry_of ( ABCD );
   DA .cut_from_bdry_of ( ABCD );
   \cinza{// at this point, ABCD is a cell whose boundary is incomplete}
   \cinza{// it has only two sides and an opening}
   \cinza{// however, it still is part of 'msh'}

   \verm{Cell} \azul{AC} ( \taag::segment, A .reverse(), C );
   AC .reverse() .glue_on_bdry_of ( ABCD );
   \cinza{// at this point, and in spite of its name, ABCD is no longer a square}
   \cinza{// it is a triangle, still part of 'msh'}

   \verm{Cell} \azul{CDA} ( \taag::triangle, CD, DA, AC );
   CDA .add_to ( msh );
\end{Verbatim}



          %--------------------------------%
\section{~~Modifying the mesh within a loop}\label{\numb section 10.\numb parag 3}
          %--------------------------------%

As is the case with many {\tt C++} containers, it is not safe to modify a mesh while
iterating over its cells.
For instance, code below gives undefined behaviour (often, a {\small\tt segmentation fault}
will occur).

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=incorrect code !,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
   \verm{Mesh}::Iterator \azul{it} = msh .iterator ( \taag::over_cells_of_dim, \laranja{2} );
   for ( it .reset(); it .in_range(); it++ )
   \{  \verm{Cell} \azul{square} = *it;
      if ( \cinza{... some criterion ...} )
      \{  square .remove_from ( msh );
         \cinza{// statement above opens the path to disaster}
         \cinza{// next time 'it' is incremented, we get undefined behaviour}
         \cinza{// we may add other cells afterwards, at no good}
         some_other_cell .add_to ( msh );     \}   \}
\end{Verbatim}

We can circumvent this problem by creating a list of cells to be cut.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 10.\numb parag 3}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
   std::list < \verm{Cell} > list_of_squares;

   \verm{Mesh}::Iterator \azul{it} = msh .iterator ( \taag::over_cells_of_dim, 2 );
   for ( it .reset(); it .in_range(); it++ )
   \{  \verm{Cell} \azul{square} = *it;
      if ( \cinza{ \mbox{\fontfamily{helvetica}\selectfont{}some criterion} } )  \cinza{// we decide to cut this square in halves}
         list_of_squares .push_back ( square );  \}

   for ( std::list < \verm{Cell} > ::iterator \azul{it_square} = list_of_squares .begin(),
         it_square != list_of_squares .end(); it_square               )
   \{  \verm{Cell} \azul{square} = * it_square;
      \cinza{// according to the same criterion, choose a diagonal, here we cut along PR}
      square .remove_from ( msh );
      \verm{Cell} \azul{PR} ( \taag::segment, P .reverse(), R );
      \verm{Cell} \azul{PQR} ( \taag::triangle, PQ, QR, PR .reverse() );
      PQR .add_to ( msh );
      \verm{Cell} \azul{RSP} ( \taag::triangle, RS, SP, PR );
      RSP .add_to ( msh );                                \}
\end{Verbatim}


          %-----------------------------%
\section{~~Improving a mesh of triangles}\label{\numb section 10.\numb parag 4}
          %-----------------------------%

Sometimes the frontal meshing algorithm, presented in section \ref{\numb section 3},
produces imperfect meshes.
Recall that this algorithm produces meshes of triangles.
For such a mesh to be considered of good quality, one may require each inner vertex
to have six neighbour triangles, and thus six neighbour segments.
This is not realistic for arbitrary domains, but it is reasonable to require each vertex
to have five, six or seven neighbours.
In most cases, the algorithm fulfills this condition,
but in rare cases we can see vertices with four or eight neighbours.

The code presented in this section searches for vertices with four (or three) neighbours,
as well as vertices with eight (or more) neighbours.
It then performs a ``flip'' operation in order to fix this undesired configuration.
Figure \ref{\numb section 10.\numb fig 2} illustrates this operation.
On the right hand side, we see that the red vertex has only four neighbours.
After ``flipping'' the blue segment, the red vertex will have five neighbours.

\begin{figure}[ht] \centering
  \includegraphics[width=127mm]{two-ellipses}
  \caption{left\,: before remeshing, right\,: after remeshing}
  \label{\numb section 10.\numb fig 2}
\end{figure}

Below is the code performing the ``flip'' operation.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 10.\numb parag 3}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
inline bool \azul{flip_segment} ( \verm{Mesh} & \azul{msh}, \verm{Cell} & \azul{seg} )

\cinza{// flip 'seg' if it is inner to 'msh'}
\cinza{// equilibrates (barycenter) the four neighbour vertices}

\cinza{// return true if the segment has been flipped, false if not}

\cinza{// assumes there are only triangular cells}
	
\cinza{// assumes there is no higher-dimensional mesh "above" 'msh'}

\{  \verm{Cell} \azul{tri2} = msh .cell_in_front_of ( seg, \taag::may_not_exist );
   if ( not tri2.exists() ) return false; 
   \verm{Cell} \azul{tri1} = msh .cell_behind ( seg, \taag::may_not_exist );
   if ( not tri1 .exists() ) return false;
   \cinza{// or, equivalently :  if ( not seg .is_inner_to ( msh ) ) return false}

   \verm{Cell} \azul{A} = seg .base() .reverse();
   \verm{Cell} \azul{B} = seg .tip();
   \verm{Cell} \azul{BC} = tri1 .boundary() .cell_in_front_of ( B, \taag::surely_exists );
   \verm{Cell} \azul{CA} = tri1 .boundary() .cell_behind ( A, \taag::surely_exists );
   \verm{Cell} \azul{AD} = tri2 .boundary() .cell_in_front_of ( A, \taag::surely_exists );
   \verm{Cell} \azul{DB} = tri2 .boundary() .cell_behind ( B, \taag::surely_exists );
   \verm{Cell} \azul{C} = BC .tip();
   assert ( CA .base() .reverse() == C );
   \verm{Cell} \azul{D} = AD.tip();
   assert ( DB .base() .reverse() == D );
	
   B .cut_from_bdry_of ( seg, \taag::do_not_bother );
   A .reverse() .cut_from_bdry_of ( seg, \taag::do_not_bother );
   \cinza{// at this point, 'seg' is a weird segment with no extremities at all}
     
   CA .cut_from_bdry_of ( tri1, \taag::do_not_bother );
   DB .cut_from_bdry_of ( tri2, \taag::do_not_bother );
   \cinza{// 'tri1' and 'tri2' now have as boundaries open chains of two segments only}
   \cinza{// even worse : one of these segments is 'seg' which is itself incomplete}
   \cinza{//              so these chains are actually disconnected}
   
   C .reverse() .glue_on_bdry_of ( seg, \taag::do_not_bother );
   D .glue_on_bdry_of ( seg, \taag::do_not_bother );
   \cinza{// 'seg' is complete again}
   \cinza{// boundaries of 'tri1' and 'tri2' are now connected chains of two segments each}
   
   DB .glue_on_bdry_of ( tri1, \taag::do_not_bother );
   CA .glue_on_bdry_of ( tri2, \taag::do_not_bother );
   \cinza{// 'tri1' and 'tri2' are complete again, their boundaries are closed loops}

   tri1 .boundary() .closed_loop ( B );
   tri2 .boundary() .closed_loop ( A );

   if ( A .is_inner_to ( msh ) ) msh .barycenter ( A );
   if ( B .is_inner_to ( msh ) ) msh .barycenter ( B );
   if ( C .is_inner_to ( msh ) ) msh .barycenter ( C );
   if ( D .is_inner_to ( msh ) ) msh .barycenter ( D );

   return true;

\}  \cinza{// end of  flip_segment}
\end{Verbatim}

Code above follows the approach, already used in paragraph \ref{\numb section 10.\numb parag 2},
of destroying partially cells (segments and triangles) and then completing them later
in a different configuration.
In contrast, paragraphs \ref{\numb section 10.\numb parag 1} and
\ref{\numb section 10.\numb parag 3} show code which simply removes cells from the mesh
then builds entirely new cells and adds them to the mesh.

In the above code, we add a {\small\tt\taag::do\_\,not\_\,bother} as argument
to methods {\small\tt cut\_\,from\_\,bdry\_\,of} and {\small\tt glue\_\,on\_\,bdry\_\,of}.
This tag changes the behaviour of the respective methods when a
{\small\tt\verm{Mesh}::Connected::OneDim} is involved
(paragraph \ref{\numb section 12.\numb parag 6} gives details on different kinds of meshes).
Recall that boundaries of triangles (and of other two-dimensional cells) are
{\small\tt\verm{Mesh}::Connected:: ::OneDim} if built through the usual constructors.

Methods {\small\tt cut\_\,from\_\,bdry\_\,of} and {\small\tt glue\_\,on\_\,bdry\_\,of},
if invoked without the {\small\tt\taag::do\_\,not\_\,bother},
when trying to add or remove cells from a {\small\tt\verm{Mesh}::Connected::OneDim},
take care to leave that mesh in a consistent state.
Removing a cell from a closed loop transforms it into an open chain of segments;
the reverse may happen when adding a cell.
Also, the number of component segments must be kept up-to-date.

If we provide the {\small\tt\taag::do\_\,not\_\,bother} to these methods,
they will not waste computing time with such details.
The respective meshes will be left in an inconsistent state.
That's why we use the method {\small\tt closed\_\,loop} and provide a vertex;
this method fixes the state of the mesh.
We could also provide the updated number of segments as second argument to method
{\small\tt closed\_\,loop}; this is not necessary in this case because the number of
segments has not changed.


          %-------------------------%
\section{~~Evolution of an interface}\label{\numb section 10.\numb parag 5}
          %-------------------------%

This example is not directly related to remeshing;
however, it is a good example of how the navigation in a mesh is important for shape optimization.

We consider a square body with a round inclusion.
The thermal conductivity $ \sigma $ of the body is constant in each region
(lower in the inclusion, higher in the bulk).
We solve a non-homogeneous Dirichlet problem in this domain, then apply a few steps of
shape optimization in order to maximize the functional

$$ J = \int\!\!\!\!\!\int_Q \sigma\, 
{\partial u \over \partial x_i}\, {\partial u \over \partial x_i} \,da $$

\noindent
with a constraint on the volume of the bulk region, given by the variable {\small\tt Lagr}.
In this paragraph, we use the convention that repeated indices sum over $ \{1,2\} $.

It is a (difficult) mathematical exercise to show that, for an infinitesimal deformation
of the geometry of our body, given by the vector field $F$, the corresponding variation
in the value of $J$ is

$$ \delta J = \int_\Gamma \Bigl[ \sigma\, 
{\partial u \over \partial x_i}\, {\partial u \over \partial x_i} \Bigr]\, 
F^k  n_k \,ds - 2 \! \int_\Gamma\! \sigma\, {\partial u \over 
  \partial x_i}\, n_i \Bigl[{\partial u \over \partial x_k} \Bigr]\, F^k ds $$
\vskip 10pt

\noindent
In the above, $ \vec n = (n_1, n_2) $ is the normal vector to the interface $ \Gamma $
(it is irrelevant if $ \vec n $ points inside the inclusion or towards the bulk);
the right parentheses stand for the
jump of the quantity inside them, that is, the value on the side of the base of $ \vec n $
minus the value on the side of $ \Gamma $ where $ \vec n $ points.

\begin{figure}[ht]  \centering
  \psfrag{p}{\small\tt\textcolor{textindraw}{P}}
  \psfrag{q}{\small\tt\textcolor{textindraw}{Q}}
  \psfrag{r}{\small\tt\textcolor{textindraw}{R}}
  \psfrag{c}{\small\tt\textcolor{textindraw}{circle}}
  \psfrag{cr}{\small\tt\textcolor{textindraw}{circle .reverse()}}
  \psfrag{d}{\small\tt\textcolor{textindraw}{disk}}
  \psfrag{i}{(inclusion)}
  \psfrag{s}{\small\tt\textcolor{textindraw}{square\_\,with\_\,round\_\,hole}}
  \psfrag{b}{(bulk)}
  \includegraphics[width=120mm]{square-split}
  \caption{interface between bulk and inclusion}
  \label{\numb section 10.\numb fig 3}
\end{figure}

Since \maniFEM{} is built around the concept of oriented cell, it is easy to control
the direction of the normal vector and the meaning of the jump.
We take a vertex {\small\tt Q} belonging to {\small\tt circle} and we know exactly
the segment ``behind'' {\small\tt Q} and the one ``in front of'' {\small\tt Q}\,:

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 10.\numb parag 5}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
   \verm{Mesh}::Iterator \azul{it} = circle .iterator ( \taag::over_vertices );
   for ( it .reset(); it .in_range(); it++ )

   \{  \verm{Cell} \azul{Q} = *it;
      \verm{Cell} \azul{PQ} = circle .cell_behind (Q);
      \verm{Cell} \azul{QR} = circle .cell_in_front_of (Q);
      \verm{Cell} \azul{P} = PQ .base() .reverse();
      \verm{Cell} \azul{R} = QR .tip();

      \cinza{// compute normal vector, pointing inside 'disk'}
      double \azul{n_x} = y(P) - y(R), \azul{n_y} = x(R) - x(P);
      double \azul{norm} = std::sqrt ( n_x * n_x + n_y * n_y );
      n_x /= norm;  n_y /= norm;
\end{Verbatim}

For computing the jump of the derivatives of the solution $u$, we use the average value
on triangles lying on each side of {\small\tt circle}.
To achieve this, we can use an iterator around a vertex, described in paragraph
\ref{\numb section 9.\numb parag 10}.

\begin{Verbatim}[commandchars=\\\{\},formatcom=\small\tt,frame=single,
   label=parag-\ref{\numb section 10.\numb parag 5}.cpp,rulecolor=\color{moldura},
   baselinestretch=0.94,framesep=2mm                                              ]
      \cinza{// compute the average value of grad temp on one side of 'circle'}
      \cinza{// (in 'disk') (in the inclusion)}
      double \azul{avrg_dtemp_dx_incl} = \laranja{0.}, \azul{avrg_dtemp_dy_incl} = \laranja{0.};
      double \azul{area} = \laranja{0.};
      \verm{Mesh}::Iterator \azul{it_incl} =
         disk .iterator ( \taag::over_cells, \taag::of_max_dim, \taag::around, Q );
      for ( it_incl .reset(); it_incl .in_range(); it_incl ++ )
      \{  \verm{Cell} \azul{tri} = * it_incl;
         fe .dock_on ( tri );
         double \azul{dtemp_dx_on_tri}, \azul{dtemp_dy_on_tri};
         compute_integral_of_grad_on_cell_in_2D
            ( temperature, tri, fe, numbering, dtemp_dx_on_tri, dtemp_dy_on_tri );
         \cinza{// 'fe' is already docked on 'tri' so this will be the domain of integration}
         double \azul{area_tri} = fe .integrate ( \laranja{1.} );
         area += area_tri;
         avrg_dtemp_dx_incl += dtemp_dx_on_tri;
         avrg_dtemp_dy_incl += dtemp_dy_on_tri;
         seg = tri .boundary() .cell_in_front_of (Q) .reverse();                   \}
      avrg_dtemp_dx_incl /= area;
      avrg_dtemp_dy_incl /= area;
\end{Verbatim}

\begin{figure}[ht] \centering
\begin{subfigure}{45mm}\centering
  \includegraphics[width=40mm]{square-interf-00}
\end{subfigure}  
\begin{subfigure}{45mm}\centering
  \includegraphics[width=40mm]{square-interf-01}
\end{subfigure}  
\begin{subfigure}{45mm}\centering
  \includegraphics[width=40mm]{square-interf-02}
\end{subfigure}  
  \caption{steps 0, 1 and 2 of a shape optimization process}
  \label{\numb section 10.\numb fig 4}
\end{figure}

Figures \ref{\numb section 10.\numb fig 4} and \ref{\numb section 10.\numb fig 5}
show five steps of the shape optimization process.
In this paragraph, we choose to remesh from scratch after each deformation of the {\small\tt circle}.
Another possibility would be to deform the existing mesh by moving its vertices
according to the vector field $F$.
For this, one would need to extend $F$ from the interface {\small\tt circle} to the
{\small\tt entire\_square}, typically by solving another, auxiliary, partial differential equation.

The inclusion becomes elongated in the direction $ (1,2) $ which is the average gradient
of the temperature, due to the non-homogeneous Dirichlet boundary condition
$ u(x,y) = x+2y $.

\begin{figure}[ht] \centering
\begin{subfigure}{45mm}\centering
  \includegraphics[width=40mm]{square-interf-03}
\end{subfigure}  
\begin{subfigure}{45mm}\centering
  \includegraphics[width=40mm]{square-interf-04}
\end{subfigure}  
\begin{subfigure}{45mm}\centering
  \includegraphics[width=40mm]{square-interf-05}
\end{subfigure}  
  \caption{steps 3, 4 and 5 of a shape optimization process}
  \label{\numb section 10.\numb fig 5}
\end{figure}

Note that some of the segments of {\small\tt circle} are becoming longer than the average
segment size of the mesh.
This makes it difficult for \maniFEM{} to build a mesh of triangles, starting from {\small\tt circle},
with an approximately constant segment size.
Thus, this approach is limited to just a few optimization steps;
after some more steps, the frontal mesh generation process will stop with some error message.
