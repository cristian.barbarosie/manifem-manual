

   element = FiniteElement("Lagrange", tetrahedron, 1)
   v = TestFunction(element)
   u = TrialFunction(element)

   #  tetrahedron = PQRS
   #  P ( coord[0], coord[1],  coord[2]  )
   #  Q ( coord[3], coord[4],  coord[5]  )
   #  R ( coord[6], coord[7],  coord[8]  )
   #  S ( coord[9], coord[10], coord[11] )

	# case 3
   a = ( 11 * u.dx(0) + 13 * u.dx(1) + 17 * u.dx(2) ) * dx   
   form = compile_form ( a )


    PQ_x = J_c0 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[3] * FE8_C0_D001_Q1[0][0][1];
    PQ_y = J_c3 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[4] * FE8_C0_D001_Q1[0][0][1];
    PQ_z = J_c6 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[5] * FE8_C0_D001_Q1[0][0][1];
    PR_x = J_c1 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[6] * FE8_C0_D001_Q1[0][0][1];
    PR_y = J_c4 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[7] * FE8_C0_D001_Q1[0][0][1];
    PR_z = J_c7 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[8] * FE8_C0_D001_Q1[0][0][1];
    PS_x = J_c2 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[9] * FE8_C0_D001_Q1[0][0][1];
    PS_y = J_c5 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[10] * FE8_C0_D001_Q1[0][0][1];
    PS_z = J_c8 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[11] * FE8_C0_D001_Q1[0][0][1];
    
    sp[0] = PR_y * PS_z
    sp[1] = PS_y * PR_z
    PRSyz = sp[2] = sp[0] - sp[1] = PR_y * PS_z - PS_y * PR_z
    sp[3] = PQ_x * PRSyz
    sp[4] = PS_y * PQ_z
    sp[5] = PQ_y * PS_z
    det_PQSyz = sp[6] = sp[4] - sp[5] = PS_y * PQ_z - PQ_y * PS_z
    sp[7] = PR_x * det_PQSyz
    sp[8] = sp[3] + sp[7] = PQ_x * PRSyz + PR_x * det_PQSyz
    sp[9] = PQ_y * PR_z
    sp[10] = PR_y * PQ_z
    det_PQRyz = sp[11] = sp[9] - sp[10] = PQ_y * PR_z - PR_y * PQ_z
    sp[12] = PS_x * det_PQRyz
    det = sp[13] = sp[8] + sp[12] = PQ_x * PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz
	 
    sp[14] = sp[2] / sp[13] = PRSyz / det
    sp[15] = - PQ_y * PS_z
    det_PQSyz = sp[16] = sp[4] + sp[15] = PS_y * PQ_z - PQ_y * PS_z
    sp[17] = sp[16] / sp[13] = det_PQSyz / det
    sp[18] = sp[11] / sp[13] = det_PQRyz / det
    sp[19] = PS_x * PR_z
    sp[20] = - PS_z * PR_x
    det_PRSxz = sp[21] = sp[19] + sp[20] = PS_x * PR_z - PS_z * PR_x
    sp[22] = sp[21] / sp[13] = det_PRSxz / det
    sp[23] = PQ_x * PS_z
    sp[24] = - PQ_z * PS_x
    det_PQSxz = sp[25] = sp[23] + sp[24] = PQ_x * PS_z - PQ_z * PS_x
    sp[26] = sp[25] / sp[13] = det_PQSxz / det
    sp[27] = PR_x * PQ_z
    sp[28] = PQ_x * PR_z
    det_PQRxz = sp[29] = sp[27] - sp[28] = PR_x * PQ_z - PQ_x * PR_z
    sp[30] = sp[29] / sp[13] = det_PQRxz / det
    sp[31] = 13 * sp[22] + 11 * sp[14];
    sp[32] = 13 * sp[26] + 11 * sp[17];
    sp[33] = 11 * sp[18] + 13 * sp[30];
    sp[34] = PR_x * PS_y
    sp[35] = PS_x * PR_y
    det_PRSxy = sp[36] = sp[34] - sp[35] = PR_x * PS_y - PS_x * PR_y
    sp[37] = sp[36] / sp[13] = det_PRSxy / det
    sp[38] = PS_x * PQ_y
    sp[39] = PQ_x * PS_y
    det_PQSxy = sp[40] = sp[38] - sp[39] = PS_x * PQ_y - PQ_x * PS_y
    sp[41] = sp[40] / sp[13] = det_PQSxy / det
    sp[42] = PQ_x * PR_y
    sp[43] = PR_x * PQ_y
    det_PQRxy = sp[44] = sp[42] - sp[43] = PQ_x * PR_y - PR_x * PQ_y
    sp[45] = sp[44] / sp[13] = det_PQRxy  / det
    sp[46] = sp[31] + 17 * sp[37];
    sp[47] = sp[32] + 17 * sp[41];
    sp[48] = sp[33] + 17 * sp[45];
    sp[49] = std::abs(sp[13]);
    sp[50] = sp[46] * sp[49];
    sp[51] = sp[47] * sp[49];
    sp[52] = sp[48] * sp[49];

so, FFC first divides everything by det then multiplies by abs(det)
what a waste of execution time !

    A[0] = -0.1666666666666667 * sp[50] - 0.1666666666666667 * sp[51] - 0.1666666666666667 * sp[52];
    A[1] = 0.1666666666666667 * sp[50];
    A[2] = 0.1666666666666667 * sp[51];
    A[3] = 0.1666666666666667 * sp[52];

11* (deriv x)
A[0] = - det_PRSyz - det_PQSyz - det_PQRyz
A[1] = det_PRSyz = sp[14]
A[2] = det_PQSyz = sp[17]
A[3] = det_PQRyz = sp[18]

13* (deriv y)
A[0] = - det_PRSxz - det_PQSxz - det_PQRxz
A[1] = det_PRSxz
A[2] = det_PQSxz
A[3] = det_PQRxz

17* (deriv z)
A[0] = - det_PRSxy - det_PQSxy - det_PQRxy
A[1] = det_PRSxy
A[2] = det_PQSxy
A[3] = det_PQRxy

